<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $table = 'pekerjaan';
    protected $fillable = [
        'customer_id',
        'perangkat_id',
        'user_id',
        'keahlian_id',
        'bagian',
        'nama_pekerjaan',
        'kategori',
        'status',
        'foto_selfie',
        'foto_kerja',
        'report_fisik',
        'keterangan',
        'start_pekerjaan',
        'end_pekerjaan',
    ];

    public function customer()
    {
      return $this->belongsTo(Customer::class);
    }
    public function perangkat()
    {
      return $this->belongsTo(Perangkat::class);
    }
    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
