<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontrak extends Model
{
    protected $table = 'kontrak';
    protected $fillable = [
        'customer_id',
        'no_kontrak',
        'periode',
        'start_kontrak',
        'end_kontrak',
        'foto_jadwal',
    ];

    public function customer()
    {
      return $this->belongsTo(Customer::class);
    }
}
