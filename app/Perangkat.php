<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perangkat extends Model
{
    protected $table = 'perangkat';
    protected $fillable = [
        'kategori',
        'perangkat',
    ];

    public function pekerjaan()
    {
      return $this->hasMany(Pekerjaan::class);
    }
    
    public function user()
    {
      return $this->hasMany(User::class);
    }
    
}
