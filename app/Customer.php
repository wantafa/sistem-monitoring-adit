<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customer';
    protected $fillable = [
        'nama_perusahaan',
        'nama_bagian',
        'alamat',
        'no_telp',
    ];

    public function kontrak()
    {
      return $this->hasMany(Kontrak::class);
    }
    public function pekerjaan()
    {
      return $this->hasMany(Pekerjaan::class);
    }
}
