<?php

namespace App\Http\Controllers;

use App\User;
use App\Customer;
use App\Pekerjaan;
use App\Perangkat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PekerjaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'teknisi') {

            $pekerjaan = Pekerjaan::where('user_id', '=', Auth::user()->id)->get();
            $customer = Customer::all();
            $perangkat = Perangkat::all();
            $no = 1;
            $user = User::where('role','teknisi')->get();
        } else {
            $pekerjaan = Pekerjaan::all();
            $customer = Customer::all();
            $perangkat = Perangkat::all();
            $user = User::where('role','teknisi')->get();
            $no = 1;
        }

        if(Auth::user()->role == 'admin') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        return view('pekerjaan.index', compact('pekerjaan','no','customer','perangkat','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'customer_id' => 'required',
            'perangkat_id' => 'required',
            'bagian' => 'required',
            'nama_pekerjaan' => 'required',
            'kategori' => 'required',
            'start_pekerjaan' => 'required',
            'end_pekerjaan' => 'required',
        ]);

        $data = $request->all();

        Pekerjaan::create($data);
    
        alert()->background('#191C24')->success('Berhasil.','Data Pekerjaan ditambahkan!');
        return redirect('pekerjaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pekerjaan  $pekerjaan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pekerjaan = Pekerjaan::find($id);
        return view('pekerjaan.show', compact('pekerjaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pekerjaan  $pekerjaan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::all();
        $perangkat = Perangkat::all();
        $pekerjaan = Pekerjaan::find($id);
        $teknisi = User::where('role','teknisi')->get();
        return view('pekerjaan.edit', compact('pekerjaan','perangkat','customer','teknisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pekerjaan  $pekerjaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'customer_id' => 'required',
            'perangkat_id' => 'required',
            'bagian' => 'required',
            'nama_pekerjaan' => 'required',
            'kategori' => 'required',
            'start_pekerjaan' => 'required',
            'end_pekerjaan' => 'required',
        ]);
        
        Pekerjaan::where('id',$id)->update([
            'user_id' => $request->user_id,
            'customer_id' => $request->customer_id,
            'perangkat_id' => $request->perangkat_id,
            'keahlian_id' => $request->keahlian_id,
            'bagian' => $request->bagian,
            'nama_pekerjaan' => $request->nama_pekerjaan,
            'kategori' => $request->kategori,
            'start_pekerjaan' => $request->start_pekerjaan,
            'end_pekerjaan' => $request->end_pekerjaan,
        ]);
    }

    public function spv_form_ambil($id)
    {
        $pekerjaan = Pekerjaan::find($id);
        // $teknisi = User::where('role','teknisi')->get();
        $teknisi = DB::table('users')
        ->join('keahlian', 'users.keahlian_id', '=', 'keahlian.id')
        ->where('users.role', '=', 'teknisi')
        ->select('users.*','keahlian.keahlian')->get();;
        return view('pekerjaan.spv_ambil', compact('pekerjaan','teknisi'));
    }

    public function form_hasil_kerja($id)
    {
        $pekerjaan = Pekerjaan::find($id);
        return view('pekerjaan.upload_hasil_kerja', compact('pekerjaan'));
    }

    public function teknisi_ambil($id)
    {
      Pekerjaan::where('id',$id)->update([
          'user_id' => Auth::user()->id,
      ]);
      alert()->background('#191C24')->success('Berhasil.','Pekerjaan Telah diambil!');
      return redirect('dashboard');
    }
    
    public function teknisi_mulai_kerja($id)
    {
    
        $pekerjaan = Pekerjaan::where('id',$id)->update([
          'user_id' => Auth::user()->id,
          'status' => 'On Progress'
      ]);

      User::where('id',Auth::user()->id)->update([
        'status' => 'kerja',
        ]);

      alert()->background('#191C24')->success('Berhasil.','Pekerjaan Telah dimulai!');
      return redirect('dashboard');
    }
    
    public function spv_ambil(Request $request, $id)
    {
      
      $pekerjaan = Pekerjaan::where('id',$id)->update([
          'user_id' => $request->user_id,
      ]);

      User::where('id',$request->user_id)->update([
        'status' => 'kerja',
        ]);

      return redirect('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pekerjaan  $pekerjaan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pekerjaan = Pekerjaan::find($id);
        $pekerjaan->delete();
        return redirect('pekerjaan');
    }

    public function laporan(Request $request)
    {
        $no = 1;
        $bulan = $request->get('bulan');
	    $tahun = $request->get('tahun');
	    $customer = $request->get('customer_id');
	    $kategori = $request->get('kategori');
        
        $customer_get = Customer::all();

        $laporan = Pekerjaan::whereYear('created_at', $tahun)
        ->whereMonth('created_at', $bulan)
        ->where('customer_id', $customer)
        ->where('kategori', $kategori)
        ->get();
        return view('pekerjaan.laporan', compact('customer_get', 'laporan', 'no'));
    }

    public function laporan_foto($id)
    {
       $pekerjaan = Pekerjaan::find($id);
        return view('pekerjaan.show_laporan', compact('pekerjaan'));
    }
    
    public function cetak_laporan($id)
    {
        $pekerjaan = Pekerjaan::find($id);
        return view('pekerjaan.cetak_laporan', compact('pekerjaan'));
    }
}
