<?php

namespace App\Http\Controllers;

use App\User;
use App\Pekerjaan;
Use Alert;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        toast('Login Berhasil', 'success')->position('center')->width('300px')->timerProgressBar()->background('#FFFFFF');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $teknisi = User::where('status','ada')->count();
        $pekerjaan_selesai = Pekerjaan::where('status','Selesai')->count();
        $pekerjaan_upcoming = Pekerjaan::where('status','Upcoming')->count();
        $teknisi_semua = User::where('role','teknisi')->count();
        $teknisi_semua1 = User::where('role','teknisi')->get();
        $pekerjaan = Pekerjaan::select('user_id')->get();

        $pekerjaan_berjalan_pm = Pekerjaan::where('kategori', 'Preventive Maintenance')
                                        ->where('start_pekerjaan', '<=', Carbon::now()->format('m/d/Y'))
                                        // ->where('perangkat_id', '>=', Carbon::now()->format('m/d/Y'))
                                        ->whereIn('status',['Upcoming','On Progress'])
                                        ->orderBy('updated_at', 'desc')
                                        ->get();
        $pekerjaan_berjalan_cm = Pekerjaan::where('kategori', 'Corrective Maintenance')
                                        ->where('start_pekerjaan', '<=', Carbon::now()->format('m/d/Y'))
                                        // ->where('end_pekerjaan', '>', Carbon::now()->format('m/d/Y'))
                                        ->whereIn('status',['Upcoming','On Progress'])
                                        ->orderBy('updated_at', 'desc')
                                        ->get();
        $pekerjaan_semua = Pekerjaan::count();
        
        // dd($pekerjaan_berjalan_pm->perangkat->kategori);

        $teknisi_upload = Pekerjaan::where('user_id', Auth::user()->id);
        $no = 1;
        $noo = 1;
        return view('index', compact('teknisi','teknisi_semua','teknisi_semua1','pekerjaan_selesai','pekerjaan_berjalan_pm', 'pekerjaan_berjalan_cm', 'pekerjaan_semua','no','noo', 'teknisi_upload', 'pekerjaan_upcoming'));
    }

    public function hasil_kerja(Request $request, $id)
    {
        $pekerjaan = Pekerjaan::find($id);
        $report_fisik = $request->file('report_fisik');
        $foto_selfie = $request->file('foto_selfie');
        $foto_kerja = $request->file('foto_kerja');
        $user = auth()->user();
 
        if (empty ($report_fisik = $request->file('report_fisik')) && empty ($foto_selfie = $request->file('foto_selfie')) && empty ($foto_kerja = $request->file('foto_kerja'))) {
     
         Pekerjaan::where('id',$id)->update([
             'report_fisik' => $request->report_fisik1,
             'foto_selfie' => $request->foto_selfie1,
             'foto_kerja' => $request->foto_kerja1,
             'keterangan' => $request->keterangan,
             'status' => 'Selesai',
             ]);
         User::where('id',$pekerjaan->user_id)->update([
             'status' => 'ada',
             ]);
        } else {
         $extension1  = $report_fisik->extension();
         $extension2  = $foto_selfie->extension();
         $extension3  = $foto_kerja->extension();
 
         if($pekerjaan->report_fisik != '/uploads/images/pekerjaan/default.png' && $pekerjaan->foto_selfie != '/uploads/images/pekerjaan/default.png' && $pekerjaan->foto_kerja != '/uploads/images/pekerjaan/default.png') {
             File::delete(public_path().$pekerjaan->report_fisik);
             File::delete(public_path().$pekerjaan->foto_selfie);
             File::delete(public_path().$pekerjaan->foto_kerja);
     
             $lokasi1 = '/uploads/images/pekerjaan/'.$user->name ."_" . "report_fisik_". time() . '.' . $extension1; 
             $lokasi2 = '/uploads/images/pekerjaan/'.$user->name ."_" . "foto_selfie_". time() . '.' . $extension2; 
             $lokasi3 = '/uploads/images/pekerjaan/'.$user->name ."_" . "foto_kerja_". time() . '.' . $extension3; 
             $request->file('report_fisik')->move("uploads/images/pekerjaan", $lokasi1);
             $request->file('foto_selfie')->move("uploads/images/pekerjaan", $lokasi2);
             $request->file('foto_kerja')->move("uploads/images/pekerjaan", $lokasi3);
             $foto1 = $lokasi1;
             $foto2 = $lokasi2;
             $foto3 = $lokasi3;
         } else {
             $foto1 = $request->report_fisik;
             $foto2 = $request->foto_selfie;
             $foto3 = $request->foto_kerja;
         }
         
             Pekerjaan::where('id',$id)->update([
                 'report_fisik' => $foto1,
                 'foto_selfie' => $foto2,
                 'foto_kerja' => $foto3,
                 'keterangan' => $request->keterangan,
                 'status' => 'Selesai',
                 ]);

            User::where('id',$pekerjaan->user_id)->update([
            'status' => 'ada',
            ]);
        }
    }
}
