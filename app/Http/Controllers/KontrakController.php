<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Kontrak;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\File;

class KontrakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'teknisi') {

            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.')->background('#191C24');
            return redirect()->to('/dashboard');
        } else {
            $kontrak = Kontrak::all();
            $customer = Customer::all();
            $no = 1;
        }
        return view('kontrak.index', compact('kontrak','no', 'customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'customer_id' => 'required',
            'no_kontrak' => 'required',
            'periode' => 'required|max:4',
            'start_kontrak' => 'required',
            'end_kontrak' => 'required',
            'foto_jadwal' => 'required',
        ]);

        $image = $request->file('foto_jadwal');
       $user = auth()->user();
       $extension  = $image->extension();

       $lokasi = '/uploads/images/jadwal/'.$user->name ."_". time() . '.' . $extension; 
       $request->file('foto_jadwal')->move("uploads/images/jadwal", $lokasi);
       $foto = $lokasi;
 

        Kontrak::create([
            'foto_jadwal' => $foto,
            'customer_id' => $request->customer_id,
            'no_kontrak' => $request->no_kontrak,
            'periode' => $request->periode,
            'start_kontrak' => $request->start_kontrak,
            'end_kontrak' => $request->end_kontrak,
        ]);

        alert()->background('#191C24')->success('Berhasil.','Data Kontrak ditambahkan!');
        return redirect('kontrak');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kontrak  $kontrak
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kontrak = Kontrak::find($id);
        return view('kontrak.show', compact('kontrak'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kontrak  $kontrak
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kontrak = Kontrak::find($id);
        $customer = Customer::all();
        return view('kontrak.edit', compact('kontrak','customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kontrak  $kontrak
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'customer_id' => 'required',
            'no_kontrak' => 'required',
            'periode' => 'required|max:4',
            'start_kontrak' => 'required',
            'end_kontrak' => 'required',
        ]);
        $cari_foto = Kontrak::find($id);
        $foto_jadwal = $request->file('foto_jadwal');
        $kontrak = $cari_foto->foto_jadwal;
        $user = auth()->user();
        
        if ($foto_jadwal){

            $extension1  = $foto_jadwal->extension();

         if($cari_foto->foto_jadwal != '/uploads/images/jadwal/default.png') {
             File::delete(public_path().$cari_foto->foto_jadwal);
     
             $lokasi = '/uploads/images/jadwal/'.$user->name ."_" . "foto_jadwal_". time() . '.' . $extension1; 
            
             $request->file('foto_jadwal')->move("uploads/images/jadwal", $lokasi);
            
            $kontrak = $lokasi;
         }

        } 
        
        Kontrak::where('id',$id)->update([
            'customer_id' => $request->customer_id,
            'no_kontrak' => $request->no_kontrak,
            'periode' => $request->periode,
            'start_kontrak' => $request->start_kontrak,
            'end_kontrak' => $request->end_kontrak,
            'foto_jadwal' => $kontrak,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kontrak  $kontrak
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kontrak = Kontrak::find($id);
        $kontrak->delete();
        return redirect('kontrak');
    }
}
