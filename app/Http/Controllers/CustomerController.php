<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'administrator' || Auth::user()->role == "admin" || Auth::user()->role == "manajer teknik") {

            $customer = Customer::all();
            $no = 1;
        } else {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.')->background('#191C24');
            return redirect()->to('/dashboard');
        }
        return view('customer.index', compact('customer','no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_perusahaan' => 'required',
            'nama_bagian' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required|max:15',
        ]);

        $data = $request->all();

        Customer::create($data);
        
        alert()->background('#191C24')->success('Berhasil.','Data Customer ditambahkan!');
        return redirect('customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        return view('customer.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_perusahaan' => 'required',
            'nama_bagian' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required|max:15',
        ]);

        Customer::where('id',$id)->update([
            'nama_perusahaan' => $request->nama_perusahaan,
            'nama_bagian' => $request->nama_bagian,
            'alamat' => $request->alamat,
            'no_telp' => $request->no_telp,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('customer');
    }
}
