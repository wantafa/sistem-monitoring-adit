<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use App\Perangkat;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PerangkatController extends Controller
{
    public function index() {
        if(Auth::user()->role == 'teknisi') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.')->background('#191C24');
            return redirect()->to('/dashboard');
        }
        $perangkat = Perangkat::all();
        $no = 1;
        return view('perangkat.index', compact('perangkat', 'no'));
    }

    public function create() {
        $no = 1;
        $response = Http::get('https://api.kawalcorona.com/indonesia/provinsi/');
        $data = $response->json();

        return view('perangkat.kopit', compact('data','no'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'kategori' => 'required',
            'perangkat' => 'required'
        ],
      [
          'kategori.required' => 'Harus diisi',
          'kategori.min' => 'Minimal 5 digit',
          'perangkat.required' => 'Harus diisi',
       ]);

        $data = $request->all();

        Perangkat::create($data);
        
        alert()->background('#191C24')->success('Berhasil.','Data Perangkat ditambahkan!');
        return redirect('perangkat');
    }

    public function show($id)
    {
        $perangkat = Perangkat::find($id);
        if(Auth::user()->role == 'user') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        return view('perangkat.show', compact('perangkat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function edit($id)
     {
         $perangkat = Perangkat::find($id);
        if(Auth::user()->role == 'user') {
            Alert::info('Oopss..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
       return view('perangkat.edit', compact('perangkat'));
     }
     
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'kategori' => 'required',
            'perangkat' => 'required|min:5'
        ]);
       
        Perangkat::where('id',$id)->update([
            'kategori' => $request->kategori,
            'perangkat' => $request->perangkat
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function destroy($id)
    {
        $item = Perangkat::findOrFail($id);
        $item->delete();
        return redirect()->route('perangkat');
    }
}
