<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Username Atau Password Salah!',
    'throttle' => 'Anda Telah Memasukkan User / Password Salah Sebanyak 6 Kali Silahkan Tunggu :seconds Detik.',

];
