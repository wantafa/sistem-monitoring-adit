<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>@yield('title') | PT. PRATAMA DATAMAKSIMA</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.base.css')}}" />
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/datatables/dataTables.bootstrap4.css')}}" />
    {{-- <link rel="stylesheet" href="{{ asset('assets/vendors/toast/toast.min.css')}}" /> --}}
    <!-- End Plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/daterange/daterange.css')}}" />
    <link rel="stylesheet" href="{{ asset('assets_old/modules/jquery-selectric/selectric.css')}}" />
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}" />
    <!-- End layout styles -->
    {{-- <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png')}}" /> --}}
</head>
@stack('page-styles')

<body>
    <div class="container-scroller">
        <!-- partial:partials/_sidebar.html -->
        @include('layouts.sidebar')
        <!-- partial -->
        <!-- partial:partials/_navbar.html -->
        <div class="container-fluid page-body-wrapper">
            @include('layouts.header')
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    @yield('content')
                </div>
                @yield('modal')
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer"><div class="d-sm-flex justify-content-center justify-content-sm-between ">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block ">Copyright © PT. PRATAMA DATAMAKSIMA</span>
                    <span class="text-muted float-none float-sm-right d-block mt-1 mt-sm-0 text-center">made with <i class="mdi mdi-heart text-danger"></i></span></div></footer>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('sweetalert::alert')
    <!-- plugins:js -->
    <script src="{{ asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap4.js')}}"></script>
    {{-- <script src="{{ asset('assets/vendors/toast/toast.min.js')}}"></script> --}}
    <script src="{{ asset('assets_old/modules/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{ asset('assets_old/js/page/modules-sweetalert.js')}}"></script>
    <script src="{{ asset('assets_old/modules/jquery-selectric/jquery.selectric.min.js')}}"></script>
    @stack('page-scripts')
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('assets/js/off-canvas.js')}}"></script>
    <script src="{{ asset('assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{ asset('assets/js/misc.js')}}"></script>
    <script src="{{ asset('assets/js/settings.js')}}"></script>
    <script src="{{ asset('assets/js/todolist.js')}}"></script>
    <!-- endinject -->

    <!-- Custom js for this page -->
    <script src="{{ asset('assets/js/data-table.js')}}"></script>
    {{-- <script src="{{ asset('assets/js/modal-demo.js')}}"></script> --}}
    <!-- End custom js for this page -->
</body>

</html>
