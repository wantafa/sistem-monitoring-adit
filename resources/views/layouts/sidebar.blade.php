@php $user = auth()->user(); @endphp
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
    <a class="sidebar-brand brand-logo" href="/"><img src="assets/images/logo.png" alt="logo" /></a>
    {{-- <a class="sidebar-brand brand-logo-mini" href="index.html"><img src="assets/images/favicon.png" alt="logo" /></a> --}}
  </div>
  <ul class="nav">
    <li class="nav-item profile">
      <div class="profile-desc">
        <div class="profile-pic">
          <div class="count-indicator">
            <img class="img-xs rounded-circle " src="{{ asset($user->image) }}" alt="">
            <span class="count bg-success"></span>
          </div>
          <div class="profile-name">
            <h5 class="mb-0 font-weight-medium">{{ Auth::user()->name }}</h5>
            <span>{{ Auth::user()->role }}</span>
          </div>
        </div>
      </div>
    </li>
    <li class="nav-item nav-category">
      <span class="nav-link"></span>
    </li>
    <li class="nav-item menu-items {{ Request::is('dashboard') ? 'active' : '' }}">
      <a class="nav-link" href="/dashboard">
        <span class="menu-icon">
          <i class="mdi mdi-speedometer"></i>
        </span>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>
    @if(Auth::user()->role == 'administrator' || Auth::user()->role == 'admin' || Auth::user()->role == 'spv' || Auth::user()->role == 'manajer teknik') 
    <li class="nav-item menu-items {{ Request::is('customer', 'perangkat','teknisi') ? 'active' : '' }}">
      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
        <span class="menu-icon">
          <i class="mdi mdi-database"></i>
        </span>
        <span class="menu-title">Data Master</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-basic">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link {{ Request::is('customer') ? 'active' : '' }}" href="{{ route('customer') }}">Data Customer</a></li>
          <li class="nav-item"> <a class="nav-link {{ Request::is('perangkat') ? 'active' : '' }}" href="{{ route('perangkat') }}">Data Perangkat</a></li>
          <li class="nav-item"> <a class="nav-link {{ Request::is('teknisi') ? 'active' : '' }}" href="{{ route('teknisi') }}">Data Teknisi</a></li>
        </ul>
      </div>
    </li>
    @endif
    @if (Auth::user()->role == 'administrator' || Auth::user()->role == 'admin' || Auth::user()->role == 'manajer teknik')
    <li class="nav-item menu-items {{ Request::is('kontrak') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('kontrak') }}">
        <span class="menu-icon">
          <i class="mdi mdi-note-text"></i>
        </span>
        <span class="menu-title">Kontrak</span>
      </a>
    </li>
    @endif
    @if (Auth::user()->role == 'teknisi' || Auth::user()->role == 'administrator' || Auth::user()->role == 'spv' || Auth::user()->role == 'manajer teknik')
    <li class="nav-item menu-items {{ Request::is('pekerjaan') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('pekerjaan') }}">
        <span class="menu-icon">
          <i class="mdi mdi-server-network"></i>
        </span>
        <span class="menu-title">Pekerjaan</span>
      </a>
    </li>
    @endif
    @if (Auth::user()->role == 'administrator' || Auth::user()->role == 'admin' || Auth::user()->role == 'manajer teknik' || Auth::user()->role == 'spv')
    <li class="nav-item menu-items {{ Request::is('laporan') ? 'active' : '' }}">
      <a class="nav-link" href="{{ route('laporan.pekerjaan') }}">
        <span class="menu-icon">
          <i class="mdi mdi-folder-multiple"></i>
        </span>
        <span class="menu-title">Laporan Pekerjaan</span>
      </a>
    </li>
    @endif
    @if (Auth::user()->role == 'administrator')
    <li class="nav-item menu-items" {{ Request::is('manajemen_user') ? 'active' : '' }}>
      <a class="nav-link" href="{{ route('manajemen_user') }}">
        <span class="menu-icon">
          <i class="mdi mdi-account-multiple"></i>
        </span>
        <span class="menu-title">Manajemen User</span>
      </a>
    </li>
    @endif
  </ul>
</nav>