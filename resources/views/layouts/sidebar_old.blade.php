<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="/">PT. Amanera Bersama Abadi</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="/">RMS</a>
      </div>
      <ul class="sidebar-menu">
          <li class="menu-header">Dashboard</li>
          <li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a class="nav-link" href="/dashboard"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a></li>
          @if (Auth::user()->role == 'admin')
          <li class="dropdown {{ Request::is('crud', 'barang') ? 'active' : '' }}">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Master Data</span></a>
            <ul class="dropdown-menu">
              <li class="{{ Request::is('crud') ? 'active' : '' }}"><a class="nav-link " href="{{ route('jancuker')}}">Data Cerita</a></li>
              <li class="{{ Request::is('barang') ? 'active' : '' }}"><a class="nav-link" href="{{ route('barang.create')}}">Data Cerita</a></li>
            </ul>
          </li>
          @endif
          <li class="menu-header">Manajemen User</li>
          <li class="dropdown {{ Request::is('manajemen_user', 'role') ? 'active' : '' }}">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>Manajemen User</span></a>
            
            <ul class="dropdown-menu">
              <li class="{{ Request::is('manajemen_user') ? 'active' : '' }}"><a class="nav-link" href="{{ route('manajemen_user.index')}}">USER</a></li>
            </ul>
          </li>
    </aside>
  </div>