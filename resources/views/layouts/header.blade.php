@php $user = auth()->user(); 
use App\Pekerjaan;
        $pekerjaan = Pekerjaan::all();
        $pekerjaan->toArray();

@endphp
 
<nav class="navbar p-0 fixed-top d-flex flex-row">
  <div class="navbar-brand-wrapper d-flex d-lg-none align-items-center justify-content-center">
    <a class="navbar-brand brand-logo-mini" href="index.html"><img src="assets/images/logo-mini.svg" alt="logo" /></a>
  </div>
  <div class="navbar-menu-wrapper flex-grow d-flex align-items-stretch">
    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
      <span class="mdi mdi-menu"></span>
    </button>
    <ul class="navbar-nav navbar-nav-right">
      <li class="nav-item dropdown">
        <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
          <i class="mdi mdi-bell"></i>
          @foreach ($pekerjaan as $item) 
         
          @if (\Carbon\Carbon::now()->format('m/d/Y') > $item->end_pekerjaan)
          <span class="count bg-danger"></span>
          
          @endif

          @if ($item->user_id)
          <span class="count bg-danger"></span>
          
          @endif
        
         @endforeach
        </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
          <h6 class="p-3 mb-0">Notifications</h6>
          <div class="dropdown-divider"></div>
          @foreach ($pekerjaan as $item) 
          @if (\Carbon\Carbon::now()->format('m/d/Y') > $item->end_pekerjaan)

          <a class="dropdown-item preview-item">
            <div class="preview-thumbnail">
              <div class="preview-icon bg-dark rounded-circle">
                <i class="mdi mdi-calendar text-danger"></i>
              </div>
            </div>
            <div class="preview-item-content">
              <p class="preview-subject mb-1">Pas Due Case [{{ $item->customer->nama_perusahaan ?? null}}]</p>
              <p class="text-muted ellipsis mb-0"> {{ $item->perangkat->kategori ?? null}} - {{ date('d M Y', strtotime($item->end_pekerjaan)) }} </p>
            </div>
          </a>
          @endif

          @if ($item->user_id)
          <a class="dropdown-item preview-item">
            <div class="preview-thumbnail">
              <div class="preview-icon bg-dark rounded-circle">
                <i class="mdi mdi-calendar text-success"></i>
              </div>
            </div>
            <div class="preview-item-content">
              <p class="preview-subject mb-1">Pekerjaan Baru | [{{ $item->customer->nama_perusahaan ?? null }}]</p>
              <p class="text-muted ellipsis mb-0"> {{ $item->perangkat->kategori ?? null }}</p>
            </div>
          </a>
          @endif

          @endforeach
          <div class="dropdown-divider"></div>

        </div>
      </li>


      <li class="nav-item dropdown">
        <a class="nav-link" id="profileDropdown" href="#" data-toggle="dropdown">
          <div class="navbar-profile" >
            <img class="img-xs rounded-circle" src="{{ asset($user->image) }}" alt="">
            <p class="mb-0 d-none d-sm-block navbar-profile-name">{{ Auth::user()->name }}</p>
            <i class="mdi mdi-menu-down d-none d-sm-block"></i>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="profileDropdown">
           <!-- <h6 class="p-3 mb-0">Profile</h6> -->
          <div class="dropdown-divider"></div>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item preview-item">
            <div class="preview-thumbnail">
              <div class="preview-icon bg-dark rounded-circle">
                <i class="mdi mdi-logout text-danger"></i>
              </div>
            </div>
            
            <div class="preview-item-content">
              {{-- <p class="preview-subject mb-1">Log out</p> --}}
              <p href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="preview-subject mb-1">Keluar</p>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
              </form>
            </div>
          </a>
        </div>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="mdi mdi-format-line-spacing"></span>
    </button>
  </div>
</nav>