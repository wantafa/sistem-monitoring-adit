 <div class="row">
     <div class="col-md-6">
         <div class="form-group">
          <input type="hidden" value="{{ $customer->id }}" id="id_data"/>
             <label>Nama Perusahaan</label> <b><span class="text-danger" id="nama_perusahaanError"></span></b>
             <input id="nama_perusahaan" name="nama_perusahaan" placeholder="Masukkan Perangkat" type="text"
                 class="form-control" value="{{$customer->nama_perusahaan}}" required>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Nama Bagian</label> <b><span class="text-danger" id="nama_bagianError"></span></b>
             <input id="nama_bagian" name="nama_bagian" placeholder="Masukkan Nama Bagian" type="text"
                 class="form-control" value="{{$customer->nama_bagian}}" required>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Alamat</label> <b><span class="text-danger" id="alamatError"></span></b>
             <input id="alamat" name="alamat" placeholder="Masukkan Alamat" type="text"
                 class="form-control" value="{{$customer->alamat}}" required>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Nomor Telepon</label> <b><span class="text-danger" id="alamatError"></span></b>
             <input id="no_telp" name="no_telp" placeholder="Masukkan Nomor Telepon" type="number"
                 class="form-control" value="{{$customer->no_telp}}" required>
         </div>
     </div>