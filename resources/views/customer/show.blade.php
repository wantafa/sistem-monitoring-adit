<div class="row">
    <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Nama Perusahaan</h6>
            <p class="mb-0">{{ $customer->nama_perusahaan }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Nama Bagian</h6>
            <p class="mb-0">{{ $customer->nama_bagian }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Alamat</h6>
            <p class="mb-0">{{ $customer->alamat }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Nomor Telepon</h6>
            <p class="mb-0">{{ $customer->no_telp }}</p>
          </li>
        </ul>
      </div>