@extends ('layouts.master')
@section('title', 'Data Customer')
@section('content')
            <div class="page-header">
              <h3 class="page-title">Customer</h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Customer</li>
                </ol>
              </nav>
            </div>
            <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#exampleModal">Tambah Data</button>
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Customer</h4>
                <div class="row">
                  <div class="col-12">
                    <div class="table-responsive">
                      <table id="order-listing" class="table text-white-50">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Perusahaan</th>
                            <th>Nama Bagian</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($customer as $item)
                          <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $item->nama_perusahaan }}</td>
                            <td>{{ $item->nama_bagian }}</td>
                            <td>
                              <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-success btn-show"><i class="mdi mdi-eye"></i></a>
                              <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-primary btn-edit"><i class="mdi mdi-table-edit"></i></a>
                              <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-danger swal-6 "><i class="mdi mdi-delete-forever"></i></a>
                              <form action="{{ route('customer.delete', $item->id) }}" id="delete{{ $item->id }}" method="post" style="display: inline-block;">
                              @method('delete')
                              @csrf        
                              </form>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
  @section('modal')

  {{-- MODAL Tambah Data--}}
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel-2">Tambah Data Customer</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('customer.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label @error('nama_perusahaan') class="text-danger" @enderror>Nama Perusahaan @error('nama_perusahaan') | {{ $message }} @enderror</label>
                <input id="nama_perusahaan" name="nama_perusahaan" placeholder="Masukkan Nama Perusahaan" type="text" class="form-control" value="{{old('nama_perusahaan')}}">

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('nama_bagian') class="text-danger" @enderror>Nama Bagian @error('nama_bagian') | {{ $message }} @enderror</label>
                <input id="nama_bagian" name="nama_bagian" placeholder="Masukkan Nama Bagian" type="text" class="form-control" value="{{old('nama_bagian')}}">

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('alamat') class="text-danger" @enderror>Alamat @error('alamat') | {{ $message }} @enderror</label>
                <input id="alamat" name="alamat" placeholder="Masukkan Alamat" type="text" class="form-control" value="{{old('alamat')}}">

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('no_telp') class="text-danger" @enderror>Nomor Telepon @error('no_telp') | {{ $message }} @enderror</label>
                <input id="no_telp" name="no_telp" placeholder="Masukkan Nomor Telepon" type="number" min="0" class="form-control" value="{{old('no_telp')}}">

              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Simpan</button>
          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>

  {{-- MODAL LIHAT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-lihat">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('store') }}" method="POST" id="form-show">
              @csrf
            <div class="modal-body">
              
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
            </div>
          </form>
        </div>
    </div>
</div>

{{-- MODAL EDIT DATA --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('store') }}" method="POST" id="form-edit" enctype="multipart/form-data">
            @method('PATCH')
              @csrf
            <div class="modal-body">

            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-update">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>
  <!-- Modal Ends -->
@endsection
@endsection
@push ('page-scripts')
@include('customer.js.customer-js')
@endpush