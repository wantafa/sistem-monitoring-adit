 <div class="row">
     <div class="col-md-12">
         <div class="form-group">
          <input type="hidden" value="{{ $pekerjaan->id }}" id="id_data"/>
             <label>Report Fisik</label>
             <br>
             <img id="frame1" class="rounded mx-auto d-block" width="200px" height="200px"/>
             <input type="file" onchange="preview1()" name="report_fisik" class="form-control border-success" style="margin-top: 20px;" value="{{old('report_fisik')}}">

            </div>
     </div>
     <div class="col-md-12">
         <div class="form-group">
             <label>Foto Dilokasi Kerja</label>
             <br>
             <img id="frame2" class="rounded mx-auto d-block" width="200px" height="200px"/>
             <input type="file" onchange="preview2()" name="foto_selfie" class="form-control border-success" style="margin-top: 20px;" value="{{old('foto_selfie')}}">
            </div>
     </div>
     <div class="col-md-12">
         <div class="form-group">
             <label>Foto Hasil Kerja</label>
             <br>
             <img id="frame3" class="rounded mx-auto d-block" width="200px" height="200px"/>
             <input type="file" onchange="preview3()" name="foto_kerja" class="form-control border-success" style="margin-top: 20px;" value="{{old('foto_kerja')}}">
            </div>
     </div>
     <div class="col-md-12">
         <div class="form-group">
             <label>Keterangan</label>
             <input name="keterangan" placeholder="Masukkan Keterangan" type="text" class="form-control border-success" value="{{old('keterangan')}}">
            </div>
     </div>
 </div>