<h1 align="center">Laporan Pekerjaan</h1>
<h1 align="center">{{ $pekerjaan->user->name ?? null}}</h1>

<style>
    .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}

.center-table {
  margin-left: auto;
  margin-right: auto;
}

</style>
<br>
<br>
<br>
<h1 align="center">Report Fisik</h1>
<img src="{{ $pekerjaan->report_fisik }}" class="center" width="782">

<table class="center-table">
    <thead>
        <tr>
            <th>Foto Selfie</th>
            <th>Foto Kerja</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><img src="{{ $pekerjaan->foto_selfie }}" width="600" height="400"></td>
            <td><img src="{{ $pekerjaan->foto_kerja }}" width="600" height="400"></td>
        </tr>
        <tr>
            <td colspan="2"><h1 align="center">{{ $pekerjaan->keterangan }}</h1></td>
        </tr>
    </tbody>
</table>

<script>
    window.print()
</script>
