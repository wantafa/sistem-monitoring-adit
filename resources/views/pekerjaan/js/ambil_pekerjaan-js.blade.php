<script>
    @if($errors->any())
        $('#exampleModal').modal('show')
    @endif
  
        $('.btn-spv').on('click', function() {
          let id = $(this).data('id')
          $.ajax({
            url:`/pekerjaan/spv_ambil/${id}`,
            method:"GET",
            success: function(data) {
              $('#modal-spv').find('.modal-body').html(data)
              $('#modal-spv').modal('show')
            },
            error:function(error){
              console.log(error)
            }
          })
        })
  
        $('.btn-spv-update').on('click', function() {
          let id = $('#form-spv').find('#id_data').val()
          var foto = $('#foto').val()
          var formData = new FormData($('#form-spv')[0]);
  
          // let formData = $('#form-spv').serialize()
          $.ajaxSetup({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
  
          $.ajax({
            type:"POST",
            url:`/pekerjaan/spv_ambil/${id}/update`,
            data:formData,
            contentType: false,
            processData: false,
            success: function(data) {
              swal('Pekerjaan Telah diambil!','', {
                    timer: 2000,
                    icon: 'success',
                });
              // $('#modal-edit').find('.modal-body').html(data)
              $('#modal-spv').modal('hide')
              window.location.assign('/dashboard')
            },
            error: function (data) {
              console.log(error)
            }
          })
        })
  
  // UPLOAD HASIL KERJA
  $('.btn-hasil-kerja').on('click', function() {
          let id = $(this).data('id')
          $.ajax({
            url:`/pekerjaan/hasil_kerja/${id}`,
            method:"GET",
            success: function(data) {
              $('#modal-hasil-kerja').find('.modal-body').html(data)
              $('#modal-hasil-kerja').modal('show')
            },
            error:function(error){
              console.log(error)
            }
          })
        })
  
        $('.btn-hasil-kerja-update').on('click', function() {
          let id = $('#form-hasil-kerja').find('#id_data').val()
          var foto = $('#foto').val()
          var formData = new FormData($('#form-hasil-kerja')[0]);
  
          // let formData = $('#form-hasil-kerja').serialize()
          $.ajaxSetup({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
  
          $.ajax({
            type:"POST",
            url:`/pekerjaan/upload_hasil_kerja/${id}`,
            data:formData,
            contentType: false,
            processData: false,
            success: function(data) {
              swal('Pekerjaan Telah Selesai!','', {
                    timer: 2000,
                    icon: 'success',
                });
              // $('#modal-edit').find('.modal-body').html(data)
              $('#modal-hasil-kerja').modal('hide')
              window.location.assign('/dashboard')
            },
            error: function (data) {
              console.log(error)
            }
          })
        })
  </script>

<script>
    function preview1() {
    frame1.src=URL.createObjectURL(event.target.files[0]);
}
  </script>

<script>
    function preview2() {
    frame2.src=URL.createObjectURL(event.target.files[0]);
}
  </script>

<script>
    function preview3() {
    frame3.src=URL.createObjectURL(event.target.files[0]);
}
  </script>
