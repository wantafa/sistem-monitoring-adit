<script>
    @if($errors->any())
        $('#exampleModal').modal('show')
    @endif
  
    $(".swal-6").click(function(e) {
      // id = e.target.dataset.id;
      id = $(this).data('id')
    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil di  Hapus :)', {
          icon: 'success',
        });
        $(`#delete${id}`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
  });
  
        $('.btn-edit').on('click', function() {
          let id = $(this).data('id')
          $.ajax({
            url:`/pekerjaan/${id}/edit`,
            method:"GET",
            success: function(data) {
              $('#modal-edit').find('.modal-body').html(data)
              $('#modal-edit').modal('show')
            },
            error:function(error){
              console.log(error)
            }
          })
        })
  
        $('.btn-update').on('click', function() {
          let id = $('#form-edit').find('#id_data').val()
          var foto = $('#foto').val()
          var formData = new FormData($('#form-edit')[0]);
  
          // let formData = $('#form-edit').serialize()
          $.ajaxSetup({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
  
          $.ajax({
            type:"POST",
            url:`/pekerjaan/update/${id}`,
            data:formData,
            contentType: false,
            processData: false,
            success: function(data) {
              swal('Data Pekerjaan Telah diubah :)','', {
                    timer: 2000,
                    icon: 'success',
                });
              // $('#modal-edit').find('.modal-body').html(data)
              $('#modal-edit').modal('hide')
              window.location.assign('/pekerjaan')
            },
            error: function (data) {
                      $('#customer_idError').addClass('d-none');
                      $('#perangkat_idError').addClass('d-none');
                      $('#user_idError').addClass('d-none');
                      $('#bagianError').addClass('d-none');
                      $('#nama_pekerjaanError').addClass('d-none');
                      $('#kategoriError').addClass('d-none');
                      $('#statusError').addClass('d-none');
                      $('#foto_selfieError').addClass('d-none');
                      $('#foto_kerjaError').addClass('d-none');
                      $('#report_fisikError').addClass('d-none');
                      $('#keteranganError').addClass('d-none');
                      $('#start_pekerjaankError').addClass('d-none');
                      $('#end_pekerjaanError').addClass('d-none');
                      var errors = data.responseJSON;
                      if($.isEmptyObject(errors) == false) {
                          $.each(errors.errors,function (key, value) {
                              var ErrorID = '#' + key +'Error';
                              $(ErrorID).removeClass("d-none");
                              $(ErrorID).text(value)
                          })
                      }
                  }
          })
        })
  
        $('.btn-show').on('click', function() {
          let id = $(this).data('id')
          $.ajax({
            url:`/pekerjaan/show/${id}`,
            method:"GET",
            success: function(data) {
              $('#modal-lihat').find('.modal-body').html(data)
              $('#modal-lihat').modal('show')
            },
            error:function(error){
              console.log(error)
            }
          })
        })

        $('.btn-laporan').on('click', function() {
          let id = $(this).data('id')
          $.ajax({
            url:`/laporan_pekerjaan/show/${id}`,
            method:"GET",
            success: function(data) {
              $('#modal-foto').find('.modal-body').html(data)
              $('#modal-foto').modal('show')
            },
            error:function(error){
              console.log(error)
            }
          })
        })
  </script>

<script>
  $(function() {
    $('input[name="start_pekerjaan"]').daterangepicker({
      singleDatePicker: true,
    showDropdowns: true,
    minYear: 2000,
    autoApply: true,
    maxYear: parseInt(moment().format('YYYY'),10)
  });

    $('input[name="end_pekerjaan"]').daterangepicker({
      singleDatePicker: true,
    showDropdowns: true,
    minYear: 2000,
    autoApply: true,
    maxYear: parseInt(moment().format('YYYY'),10)
  });
  });
  </script>

{{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
