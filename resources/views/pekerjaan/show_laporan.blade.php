<div class="row">
    <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Foto Selfie</h6>
            <img class="rounded mx-auto d-block" src="{{ $pekerjaan->foto_selfie }}" width="250" height="250">
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Foto Kerja</h6>
            <img class="rounded mx-auto d-block" src="{{ $pekerjaan->foto_kerja }}" width="250" height="250">
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Report Fisik</h6>
            <img class="rounded mx-auto d-block" src="{{ $pekerjaan->report_fisik }}" width="250" height="250">
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Keterangan</h6>
            <p class="mb-0">{{ $pekerjaan->keterangan }}</p>
          </li>
        </ul>
      </div>