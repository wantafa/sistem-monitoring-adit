@extends ('layouts.master')
@section('title', 'Laporan Pekerjaan')
@section('content')

<div class="page-header">
    <h3 class="page-title">Laporan Pekerjaan</h3>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Laporan Pekerjaan</li>
      </ol>
    </nav>
  </div>
      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'administrator' || Auth::user()->role == 'spv' || Auth::user()->role == 'manajer teknik')
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>Filter Laporan Pekerjaan</h4>
            </div>
            <div class="card-body">
    
            <form action="{{ route('laporan.pekerjaan') }}" method="GET" class="form-group" id="formFilter">
              {{ csrf_field() }}
              <select style="cursor:pointer;" class="form-control col-6 border-dark" id="tag_select" name="tahun">
              <option value="0" selected disabled> Pilih Tahun</option>
                <?php 
                $tahun = date('Y');
                $min = $tahun - 10;
                $max = $tahun;
                for( $i=$max; $i>=$min; $i-- ) {{
                echo '<option value='.$i.'>'.$i.'</option>';
                }}
                ?>
            </select>
            <select style="cursor:pointer;margin-top:1.5em;margin-bottom:1.5em;" class="form-control col-6 border-dark" id="tag_select" name="bulan">
              <option value="0" selected disabled> Pilih Bulan</option>
              <option value="01"> Januari</option>
              <option value="02"> Februari</option>
              <option value="03"> Maret</option>
              <option value="04"> April</option>
              <option value="05"> Mei</option>
              <option value="06"> Juni</option>
              <option value="07"> Juli</option>
              <option value="08"> Agustus</option>
              <option value="09"> September</option>
              <option value="10"> Oktober</option>
              <option value="11"> November</option>
              <option value="12"> Desember</option>
            </select>

            <select name="customer_id" class="form-control col-6 mb-3">
                @foreach ($customer_get as $item)
                    <option value="{{ $item->id }}">{{ $item->nama_perusahaan }}</option>
                @endforeach
            </select>

            <select name="kategori" class="form-control col-6">
                    <option value="Preventive Maintenance">Preventive Maintenance</option>
                    <option value="Corrective Maintenance">Corrective Maintenance</option>
            </select>
            </form>
            <button class="btn btn-info btn-block col-6" type="submit" form="formFilter" value="Submit">Cari Data</button>
          </div>
        </div>
      </div>
      </div>
      @endif
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4>Laporan Pekerjaan</h4>
        </div>

        
        {{-- <div class="col-md-12">
          <a href="{{ route('cetakfull.laporan', csrf_token())  }}&tahun={{ $tahun }}&bulan={{ $bulan }}" class="btn btn-info" style="float: right">Cetak <i class="fa fa-print"></i></a>
          </div> --}}
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-hover table-striped" id="table-1">
              <thead>                                 
                <tr class="table-secondary">
                  <th>No</th>
                  <th>Kategori</th>
                  <th>Nama Pekerjaan</th>
                  <th>Nama Teknisi</th>
                  <th>Perangkat</th>
                  <th>Tanggal</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>             
                @foreach ($laporan as $item)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $item->kategori ?? null}}</td>
                  <td>{{ $item->nama_pekerjaan ?? null}}</td>
                  <td>{{ $item->user->name ?? null}}</td>
                  <td>{{ $item->perangkat->perangkat ?? null}}</td>
                  <td>{{ ($item->created_at ?? null)->translatedFormat('l, d F Y')}}</td>
                  <td>{{ $item->status ?? null}}</td>
                  <td>
                  <a href="#" data-id="{{$item->id}}" class="btn btn-success btn-sm btn-laporan">Detail</a>  
                  <a href="{{ route('cetak_laporan', $item->id) }}" class="btn btn-info btn-sm">Cetak</a>  
                  </td>
                  </tr>
                @endforeach                    
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
    </section>
    @section('modal')

    {{-- MODAL LIHAT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-foto">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Laporan Pekerjaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('store') }}" method="POST" id="form-show">
              @csrf
            <div class="modal-body">
              
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
            </div>
          </form>
        </div>
    </div>
</div>
    @endsection

@endsection
@push ('page-scripts')
@include('pekerjaan.js.pekerjaan-js')
@endpush