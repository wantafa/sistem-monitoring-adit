 <div class="row">
     <div class="col-md-6">
         <div class="form-group">
          <input type="hidden" value="{{ $pekerjaan->id }}" id="id_data"/>
             <label>Nama Customer</label> <b><span class="text-danger" id="kategoriError"></span></b>
             <select name="customer_id" class="form-control">
                <option value="{{ $pekerjaan->customer_id }}">{{ $pekerjaan->customer->nama_perusahaan }}</option>
                @foreach ($customer as $item)
                <option value="{{ $item->id }}">{{ $item->nama_perusahaan }}</option>
                @endforeach
              </select>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Perangkat</label> <b><span class="text-danger" id="perangakatError"></span></b>
             <select name="perangkat_id" class="form-control">
                <option value="{{ $pekerjaan->perangkat_id }}">{{ $pekerjaan->perangkat->kategori }} - {{ $pekerjaan->perangkat->perangkat }}</option>
                @foreach ($perangkat as $item)
                  <option value="{{ $item->id }}">{{ $item->kategori }} - {{ $item->perangkat }}</option>
                @endforeach
              </select>
         </div>
     </div>
    @if (!empty($pekerjaan->user_id))
     <div class="col-md-6">
         <div class="form-group">
             <label>Teknisi</label> <b><span class="text-danger"></span></b>
             <select name="user_id" class="form-control">
                <option value="{{ $pekerjaan->user_id }}">{{ $pekerjaan->user->name }}</option>
                @foreach ($teknisi as $item)
                  <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
              </select>
         </div>
     </div>
     @endif
     <div class="col-md-6">
         <div class="form-group">
             <label>Keahlian</label> <b><span class="text-danger" id="keahlianError"></span></b>
             <select name="keahlian_id" class="form-control">
                <option value="{{ $pekerjaan->keahlian_id }}">Pilih Keahlian</option>
                  <option value="1">Mechanical</option>
                  <option value="2">Electrical</option>
                  <option value="3">Network</option>
                  <option value="4">Umum</option>
              </select>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Bagian</label> <b><span class="text-danger" id="bagianError"></span></b>
             <input id="bagian" name="bagian" placeholder="Masukkan Bagian" type="text"
                 class="form-control" value="{{$pekerjaan->bagian}}">
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Nama Pekerjaan</label> <b><span class="text-danger" id="nama_pekerjaanError"></span></b>
             <input id="nama_pekerjaan" name="nama_pekerjaan" placeholder="Masukkan Nama Pekerjaan" type="text"
                 class="form-control" value="{{$pekerjaan->nama_pekerjaan}}">
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Kategori</label> <b><span class="text-danger" id="kategoriError"></span></b>
             <select name="kategori" class="form-control">
                <option value="{{$pekerjaan->kategori}}">{{$pekerjaan->kategori}}</option>
                <option value="Preventive Maintenance">Preventive Maintenance</option>
                <option value="Corrective Maintenance">Corrective Maintenance</option>
              </select>
         </div>
     </div>
     <div class="col-md-6">
        <div class="form-group">
            <label>Mulai Pekerjaan</label> <b><span class="text-danger" id="start_pekerjaanError"></span></b>
            <input id="start_pekerjaan" name="start_pekerjaan" placeholder="Masukkan Mulai Pekerjaan" type="text"
                class="form-control" value="{{$pekerjaan->start_pekerjaan}}">
        </div>
    </div>
     <div class="col-md-6">
        <div class="form-group">
            <label>Selesai Pekerjaan</label> <b><span class="text-danger" id="selesai_pekerjaanError"></span></b>
            <input id="end_pekerjaan" name="end_pekerjaan" placeholder="Masukkan Selesai Pekerjaan" type="text"
                class="form-control" value="{{$pekerjaan->end_pekerjaan}}">
        </div>
    </div>

    <script>
        $(function() {
          $('input[name="start_pekerjaan"]').daterangepicker({
            singleDatePicker: true,
          showDropdowns: true,
          minYear: 2000,
          autoApply: true,
          maxYear: parseInt(moment().format('YYYY'),10)
        });
      
          $('input[name="end_pekerjaan"]').daterangepicker({
            singleDatePicker: true,
          showDropdowns: true,
          minYear: 2000,
          autoApply: true,
          maxYear: parseInt(moment().format('YYYY'),10)
        });
        });
        </script>