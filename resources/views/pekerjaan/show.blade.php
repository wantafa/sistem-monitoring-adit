<div class="row">
    <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Nama Customer</h6>
            <p class="mb-0">{{ $pekerjaan->customer->nama_perusahaan }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Perangkat</h6>
            <p class="mb-0">{{ $pekerjaan->perangkat->kategori }} - {{ $pekerjaan->perangkat->perangkat }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Bagian</h6>
            <p class="mb-0">{{ $pekerjaan->bagian }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Nama Pekerjaan</h6>
            <p class="mb-0">{{ $pekerjaan->nama_pekerjaan }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Kategori</h6>
            <p class="mb-0">{{ $pekerjaan->kategori }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Mulai Pekerjaan</h6>
            <p class="mb-0">{{ $pekerjaan->start_pekerjaan }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Selesai Pekerjaan</h6>
            <p class="mb-0">{{ $pekerjaan->end_pekerjaan }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Status</h6>
            <p class="mb-0">@if ($pekerjaan->status == 'Upcoming')
              <span class="badge badge-outline-info">{{ $pekerjaan->status ?? null}}</span>
              @elseif ($pekerjaan->status == 'Selesai')
              <span class="badge badge-outline-success">{{ $pekerjaan->status ?? null}}</span>
              @else
              <span class="badge badge-outline-warning">{{ $pekerjaan->status ?? null}}</span></td>
              @endif</p>
          </li>
        </ul>
      </div>