 <div class="row">
     <div class="col-md-12">
         <div class="form-group">
          <input type="hidden" value="{{ $pekerjaan->id }}" id="id_data"/>
             <label>Nama Teknisi</label>
             <select name="user_id" class="form-control text-white">
              <option value="" disabled>- Pilih Teknisi -</option>
              @foreach ($teknisi as $item)
              <option value="{{ $item->id }}">{{ $item->name }} - {{ $item->keahlian }}</option>
              @endforeach
            </select>
         </div>
     </div>
 </div>