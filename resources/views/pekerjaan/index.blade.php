@extends ('layouts.master')
@section('title', 'Data Pekerjaan')
@section('content')
<div class="page-header">
  <h3 class="page-title">Pekerjaan</h3>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
      <li class="breadcrumb-item active" aria-current="page">Pekerjaan</li>
    </ol>
  </nav>
</div>
@if (Auth::user()->role == 'spv' || Auth::user()->role == 'administrator')
<button type="button" class="btn btn-rounded btn-primary mb-2" data-toggle="modal" data-target="#exampleModal"><i class="mdi mdi-plus-circle"></i>Tambah Data</button>
@endif
<div class="card">
  <div class="card-body">
    <h4 class="card-title">Pekerjaan</h4>
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table id="order-listing" class="table text-light ">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Perusahaan</th>
                <th>Nama Teknisi</th>
                <th>Nama Pekerjaan</th>
                <th>Kategori</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($pekerjaan as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->customer->nama_perusahaan ?? null}}</td>
                <td>{{ $item->user->name ?? null}}</td>
                <td>{{ $item->nama_pekerjaan ?? null}}</td>
                <td>{{ $item->kategori ?? null}}</td>
                <td>
                  @if ($item->status == 'Upcoming')
                  <span class="badge badge-outline-info">{{ $item->status ?? null}}</span>
                  @elseif ($item->status == 'Selesai')
                  <span class="badge badge-outline-success">{{ $item->status ?? null}}</span>
                  @elseif (\Carbon\Carbon::now()->format('m/d/Y') > $item->end_pekerjaan)
                  <span class="badge badge-danger">Past Due</span>
                  @endif
                </td>
                <td>
                  <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-success btn-show"><i class="mdi mdi-eye"></i></a>
                  @if (Auth::user()->role == 'administrator' || Auth::user()->role == 'spv')
                  <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-primary btn-edit"><i class="mdi mdi-table-edit"></i></a>
                  <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-danger swal-6 "><i class="mdi mdi-delete-forever"></i></a>
                  <form action="{{ route('pekerjaan.delete', $item->id) }}" id="delete{{ $item->id }}" method="post" style="display: inline-block;">
                  @method('delete')
                  @csrf        
                  </form>
                </td>
              @endif
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @section('modal')

  {{-- MODAL Tambah Data--}}
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel-2">Tambah Data Pekerjaan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('pekerjaan.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label @error('customer_id') class="text-danger" @enderror>Nama Customer @error('customer_id') | {{ $message }} @enderror</label>
                <select name="customer_id" class="form-control">
                  <option value="" disabled>- Pilih Customer -</option>
                  @foreach ($customer as $item)
                  <option value="{{ $item->id }}">{{ $item->nama_perusahaan }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('perangkat_id') class="text-danger" @enderror>Perangkat @error('perangkat_id') | {{ $message }} @enderror</label>

                <select name="perangkat_id" class="form-control">
                  <option value="" disabled>- Pilih Perangkat -</option>
                  @foreach ($perangkat as $item)
                  <option value="{{ $item->id }}">{{ $item->kategori }} - {{ $item->perangkat }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('keahlian_id') class="text-danger" @enderror>Keahlian @error('keahlian_id') | {{ $message }} @enderror</label>
                <select name="keahlian_id" class="form-control">
                  <option value="" disabled>- Pilih Keahlian -</option>
                  <option value="1">Mechanical</option>
                  <option value="2">Electrical</option>
                  <option value="3">Network</option>
                  <option value="4">Umum</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('bagian') class="text-danger" @enderror>Bagian @error('bagian') | {{ $message }} @enderror</label>
                <input id="bagian" name="bagian" placeholder="Masukkan Bagian" type="text" class="form-control" value="{{old('bagian')}}">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('nama_pekerjaan') class="text-danger" @enderror>Nama Pekerjaan @error('nama_pekerjaan') | {{ $message }} @enderror</label>
                <input id="nama_pekerjaan" name="nama_pekerjaan" placeholder="Masukkan Nama Pekerjaan" type="text" class="form-control" value="{{old('nama_pekerjaan')}}">

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('nama_pekerjaan') class="text-danger" @enderror>Kategori @error('nama_pekerjaan') | {{ $message }} @enderror</label>
                <select name="kategori" class="form-control">
                  <option value="" disabled>- Pilih Kategori -</option>
                  <option value="Preventive Maintenance">Preventive Maintenance</option>
                  <option value="Corrective Maintenance">Corrective Maintenance</option>
                </select>

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('start_pekerjaan') class="text-danger" @enderror>Mulai Pekerjaan @error('start_pekerjaan') | {{ $message }} @enderror</label>
                <input id="start_pekerjaan" name="start_pekerjaan" type="text" class="form-control" value="{{old('start_pekerjaan')}}">

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('end_pekerjaan') class="text-danger" @enderror>Selesai Pekerjaan @error('end_pekerjaan') | {{ $message }} @enderror</label>
                <input id="end_pekerjaan" name="end_pekerjaan" type="text" class="form-control" value="{{old('end_pekerjaan')}}">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Simpan</button>
          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>

  {{-- MODAL LIHAT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-lihat">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Pekerjaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('store') }}" method="POST" id="form-show">
              @csrf
            <div class="modal-body">
              
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
            </div>
          </form>
        </div>
    </div>
</div>

{{-- MODAL EDIT DATA --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Pekerjaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('store') }}" method="POST" id="form-edit" enctype="multipart/form-data">
            @method('PATCH')
              @csrf
            <div class="modal-body">

            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-update">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>
  <!-- Modal Ends -->
@endsection
@endsection
@push ('page-scripts')
@include('pekerjaan.js.pekerjaan-js')
@endpush