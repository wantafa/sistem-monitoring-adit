<div class="row">
    <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Kategori</h6>
            <p class="mb-0">{{ $perangkat->kategori }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Perangkat</h6>
            <p class="mb-0">{{ $perangkat->perangkat }}</p>
          </li>
        </ul>
      </div>