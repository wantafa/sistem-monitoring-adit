@extends ('layouts.master')
@section('title', 'Data Perangkat')
@section('content')
            <div class="page-header">
              <h3 class="page-title">Perangkat</h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Perangkat</li>
                </ol>
              </nav>
            </div>
            <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#exampleModal">Tambah Data</button>
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Perangkat</h4>
                <div class="row">
                  <div class="col-12">
                    <div class="table-responsive">
                      <table id="order-listing" class="table text-white-50">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Kategori</th>
                            <th>Perangkat</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($perangkat as $item)
                          <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $item->kategori }}</td>
                            <td>{{ $item->perangkat }}</td>
                            <td>
                              <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-success btn-show"><i class="mdi mdi-eye"></i></a>
                              <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-primary btn-edit"><i class="mdi mdi-table-edit"></i></a>
                              <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-danger swal-6 "><i class="mdi mdi-delete-forever"></i></a>
                              <form action="{{ route('perangkat.delete', $item->id) }}" id="delete{{ $item->id }}" method="post" style="display: inline-block;">
                              @method('delete')
                              @csrf        
                              </form>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
  @section('modal')

  {{-- MODAL Tambah Data--}}
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel-2">Tambah Data Perangkat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('store') }}" method="POST" enctype="multipart/form-data">
          @csrf
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label @error('kategori') class="text-danger" @enderror>Kategori @error('kategori') | {{ $message }} @enderror</label>
                <select name="kategori" class="form-control">
                  <option value="" disabled>- Pilih Kategori -</option>
                  <option value="Mechanical">Mechanical</option>
                  <option value="Electrical">Electrical</option>
                  <option value="Plumbing">Plumbing</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('perangkat') class="text-danger" @enderror>Perangkat @error('perangkat') | {{ $message }} @enderror</label>
                <input id="perangkat" name="perangkat" placeholder="Masukkan Perangkat" type="text" class="form-control" value="{{old('perangkat')}}">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-info">Simpan</button>
          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>

  {{-- MODAL LIHAT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-lihat">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Lihat Note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('store') }}" method="POST" id="form-show">
              @csrf
            <div class="modal-body">
              
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
            </div>
          </form>
        </div>
    </div>
</div>

{{-- MODAL EDIT DATA --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('store') }}" method="POST" id="form-edit" enctype="multipart/form-data">
            @method('PATCH')
              @csrf
            <div class="modal-body">

            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-update">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>
  <!-- Modal Ends -->
@endsection
@endsection
@push ('page-scripts')
@include('perangkat.js.perangkat-js')
@endpush
