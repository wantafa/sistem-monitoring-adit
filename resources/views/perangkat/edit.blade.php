 <div class="row">
     <div class="col-md-6">
         <div class="form-group">
          <input type="hidden" value="{{ $perangkat->id }}" id="id_data"/>
             <label>Kategori</label> <b><span class="text-danger" id="kategoriError"></span></b>
             <select name="kategori" class="form-control">
                <option value="{{ $perangkat->kategori }}">{{ $perangkat->kategori }}</option>
                <option value="Mechanical">Mechanical</option>
                <option value="Electrical">Electrical</option>
                <option value="Network">Network</option>
                <option value="Umum">Umum</option>
              </select>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Perangkat</label> <b><span class="text-danger" id="perangakatError"></span></b>
             <input id="perangkat" name="perangkat" placeholder="Masukkan Perangkat" type="text"
                 class="form-control" value="{{$perangkat->perangkat}}" required>
         </div>
     </div>