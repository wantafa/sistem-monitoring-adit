@extends ('layouts.master')
@section('title', 'Data User')
@section('content')
<div class="content">
</div>

<div class="page-header">
  <h3 class="page-title">Manajemen User</h3>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
      <li class="breadcrumb-item active" aria-current="page">Manajemen User</li>
    </ol>
  </nav>
</div>

    <section class="content" style="padding-top: 5px">
      <div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
          <button class="btn btn-rounded btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="mdi mdi-plus-circle"></i>Tambah Data User</button>
            @if (session('status'))
                <div class="">
                    {{ session('status') }}
                </div>
            @endif
    </div>
      </div>

      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Manajemen User</h4>
          <div class="row">
            <div class="col-12">
          <div class="table-responsive">
            <table class="table text-light" id="order-listing">
              <thead>                                 
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Username</th>
                  <th>Role Akses</th>
                  <th>Email</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>             
                @foreach ($user as $item)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $item->name ?? null}}</td>
                  <td>{{ $item->username ?? null}}</td>
                  <td>{{ $item->role ?? null}}</td>
                  <td>{{ $item->email ?? null}}</td>
                  <td>
                  
                  <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-primary btn-sm btn-edit"><i class="mdi mdi-table-edit"></i></a>
                  <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-danger btn-sm swal-6 "><i class="mdi mdi-delete-forever"></i></a>
                  <form action="{{ route('manajemen_user.delete', $item->id) }}" id="delete{{ $item->id }}" method="post" style="display: inline-block;">
                     @method('delete')
                     @csrf        
                    </form>
                    </td>
                  </tr>
                @endforeach                    
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
    </section>

  @section('modal')

  {{-- MODAL TAMBAH DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Tambah Data User</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('manajemen_user.store') }}" method="POST">
                @csrf
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('name') class="text-danger" @enderror>Nama User @error('name') | {{ $message }} @enderror</label>
                      <input id="name" name="name" placeholder="Masukkan Nama" type="text" class="form-control" value="{{old('name')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('username') class="text-danger" @enderror>Username @error('username') | {{ $message }} @enderror</label>
                      <input id="username" name="username" placeholder="Masukkan Username" type="text" class="form-control" value="{{old('username')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('role') class="text-danger" @enderror>Role Akses @error('role') | {{ $message }} @enderror</label>
                      <select id="dynamic" name="role" class="form-control border-primary">
                        <option value="{{old('kategori')}}">-- Pilih Role --</option>
                        <option value="admin">Admin</option>
                    </select>
                    <br>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                 <label>Tambah Role Akses</label>
                    <div class="input-group mb-3">
                  <input class="form-control" type="text" id="add_val" name="role" placeholder="Masukkan Role Akses">
                  <br>
                  <div class="input-group-append">
                    <button class="btn btn-warning btn-sm" type="button" id="bt_add_val">Tambah Role</button>
                </div>
                </div>
                    </div>
                </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('email') class="text-danger" @enderror>Email @error('email') | {{ $message }} @enderror</label>
                      <input id="email" name="email" placeholder="Masukkan Email" type="email" class="form-control" value="{{old('email')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('password') class="text-danger" @enderror>Password @error('password') | {{ $message }} @enderror</label>
                      <input id="password" name="password" placeholder="Masukkan Password" type="password" class="form-control" value="{{old('password')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('keahlian_id') class="text-danger" @enderror>Keahlian @error('keahlian_id') | {{ $message }} @enderror</label>
                      <select name="keahlian_id" class="form-control">
                        <option value="">Pilih Keahlian</option>
                        <option value="1">Mechanical</option>
                        <option value="2">Electrical</option>
                        <option value="3">Plumbing</option>
                    </select>
                    </div>
                  </div>
            </div>
          </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>

              </div>
            </form>
          </div>
      </div>
  </div>

  {{-- MODAL LIHAT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-lihat">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Lihat Data User</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('manajemen_user.store') }}" method="POST" id="form-show">
                @csrf
              <div class="modal-body">
                
              </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
              </div>
            </form>
          </div>
      </div>
  </div>

  {{-- MODAL EDIT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-edit">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Edit Data User</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('manajemen_user.store') }}" method="POST" id="form-edit">
              {{-- @method('patch') --}}
                @csrf
              <div class="modal-body">

              </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  <button type="button" class="btn btn-primary btn-update">Simpan</button>
              </div>
            </form>
          </div>
      </div>
  </div>

  @endsection

@endsection
@push ('page-scripts')
@include('manajemen_user.js.user-js')
@endpush