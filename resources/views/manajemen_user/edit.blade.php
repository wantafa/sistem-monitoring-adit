 <div class="row">
     <div class="col-md-6">
         <div class="form-group">
             <input type="hidden" value="{{ $user->id }}" id="id_data" />
             <label>Nama User</label> <b><span class="text-danger" id="nameError"></span></b>
             <input id="name" name="name" placeholder="Masukkan Nama User" type="text" class="form-control"
                 value="{{$user->name}}">
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Username</label> <b><span class="text-danger" id="usernameError"></span></b>
             <input id="username" name="username" placeholder="Masukkan Username" type="text" class="form-control"
                 value="{{$user->username}}">
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Role Akses</label> 
             <select id="dinamik" name="role" class="form-control border-primary">
                <option value="{{$user->role}}">{{$user->role}}</option>
                <option value="admin">Admin</option>
            </select>
            <br>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Keahlian</label> 
             <select name="keahlian_id" class="form-control">
                        <option value="{{$user->keahlian_id}}">Pilih Keahlian</option>
                        <option value="1">Mechanical</option>
                        <option value="2">Electrical</option>
                        <option value="3">Network</option>
                        <option value="4">Umum</option>
              </select>
            <br>
         </div>
     </div>
     <div class="col-md-6">
        <div class="form-group">
     <label>Tambah Role Akses</label>
        <div class="input-group mb-5">
      <input class="form-control" type="text" id="add_valu" name="role" placeholder="Masukkan Role Akses">
      <br>
      <div class="input-group-append">
        <button class="btn btn-warning btn-sm" type="button" id="btn_add_val">Tambah Role</button>
    </div>
    </div>
        </div>
    </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Email</label> <b><span class="text-danger" id="emailError"></span></b>
             <input id="email" name="email" placeholder="Masukkan Email" type="email" class="form-control"
                 value="{{$user->email}}">
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Password</label> <b><span class="text-danger" id="passwordError"></span></b>
             <input id="password" name="password" placeholder="Masukkan Password" type="password" class="form-control"
                 value="">
         </div>
     </div>
