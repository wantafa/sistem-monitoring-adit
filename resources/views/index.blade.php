@extends ('layouts.master')
@section('title', 'Dashboard')
@section('content')
<div class="section-body">
    <!-- Main Content -->
      {{-- <div class="section-header">
        <h1>Dashboard</h1>
      </div> --}}
      <div class="alert alert-light alert-has-icon">
        <div class="alert-body">
          <div class="alert-title"><i class="mdi mdi-lightbulb"></i>Hallo, {{ Auth::user()->name }}</div>
          Selamat Datang Di Sistem Monitoring Teknisi!
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4 grid-margin">
          <div class="card">
            <div class="card-body">
              <h5>TEKNISI TERSEDIA</h5>
              <div class="row">
                <div class="col-8 col-sm-12 col-xl-8 my-auto">
                  <div class="d-flex d-sm-block d-md-flex align-items-center">
                    <h2 class="mb-0">{{ $teknisi }} Orang</h2>
                  </div>
                  <h6 class="text-muted font-weight-normal">Total Teknisi {{ $teknisi_semua }} Orang</h6>
                </div>
                <div class="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                  <i class="icon-lg mdi mdi-worker text-primary ml-auto"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 grid-margin">
          <div class="card">
            <div class="card-body">
              <h5>PEKERJAAN UPCOMING</h5>
              <div class="row">
                <div class="col-8 col-sm-12 col-xl-8 my-auto">
                  <div class="d-flex d-sm-block d-md-flex align-items-center">
                    <h2 class="mb-0">{{ $pekerjaan_upcoming }}</h2>
                  </div>
                  <h6 class="text-muted font-weight-normal">Total Semua Pekerjaan {{ $pekerjaan_semua }}</h6>
                </div>
                <div class="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                  <i class="icon-lg mdi mdi-calendar-clock text-warning ml-auto"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 grid-margin">
          <div class="card">
            <div class="card-body">
              <h5>PEKERJAAN SELESAI HARI INI</h5>
              <div class="row">
                <div class="col-8 col-sm-12 col-xl-8 my-auto">
                  <div class="d-flex d-sm-block d-md-flex align-items-center">
                    <h2 class="mb-0">{{ $pekerjaan_selesai }}</h2>
                  </div>
                </div>
                <div class="col-4 col-sm-12 col-xl-4 text-center text-xl-right">
                  <i class="icon-lg mdi mdi-check-all text-success ml-auto"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">LIST PEKERJAAN PREVENTIVE MAINTENANCE UPCOMING</h4>
              <div class="table-responsive">
                <table class="table" id="order-listing">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Customer</th>
                      <th>Perangkat</th>
                      <th>Teknisi</th>
                      <th>Nama Pekerjaan</th>
                      <th>Mulai Pekerjaan</th>
                      <th>Status</th>
                      <th>Estimasi Waktu</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($pekerjaan_berjalan_pm as $item)
                    <tr>
                      <td>{{ $no++ }}</td>
                      <td>{{ $item->customer->nama_perusahaan }}</td>
                      <td>{{ $item->perangkat->kategori }} - {{ $item->perangkat->perangkat }}</td>
                      <td>{{ $item->user->name ?? null }}</td>
                      <td>{{ $item->nama_pekerjaan }}</td>
                      <td>{{ $item->start_pekerjaan }}</td>
                      <td>
                        @if ($item->status == 'On Progress')
                        <div class="badge badge-outline-warning">On Progress</div>
                        @else
                        <div class="badge badge-outline-primary">{{ $item->status }}</div>
                        @endif
                      </td>
                      <td>
                      @if ($item->status == 'On Progress')
                      <script>
                      CountDownTimer('{{$item->created_at}}', 'countdown');
                      function CountDownTimer(dt, id)
                      {
                        var end = new Date('{{$item->updated_at}}');
                        end.setHours( end.getHours() + 3 ); 
                        var _second = 1000;
                        var _minute = _second * 60;
                        var _hour = _minute * 60;
                        var _day = _hour * 24;
                        var timer;
                        function showRemaining() {
                          var now = new Date();
                          var distance = end - now;
                          if (distance < 0) {

                            clearInterval(timer);
                            document.getElementById(id).innerHTML = '<div class="badge badge-danger">Waktu Sudah Habis</div>';
                            return;
                          }
                          var days = Math.floor(distance / _day);
                          var hours = Math.floor((distance % _day) / _hour);
                          var minutes = Math.floor((distance % _hour) / _minute);
                          var seconds = Math.floor((distance % _minute) / _second);

                          document.getElementById(id).innerHTML = days + ' Hari ';
                          document.getElementById(id).innerHTML += hours + ' Jam ';
                          document.getElementById(id).innerHTML += minutes + ' Menit ';
                          document.getElementById(id).innerHTML += seconds + ' Detik';
                          document.getElementById(id).innerHTML +='<h6 class="text-warning">Sisa Waktu Pengerjaan</h6>';
                        }
                        timer = setInterval(showRemaining, 1000);
                      }
                    </script>
                    <div id="countdown">
                    @else
                    3 Jam 0 Menit 0 Detik
                    @endif 
                      </td>
                      <td>
                        @if (Auth::user()->role == 'teknisi' && empty($item->user->name) && $item->keahlian_id == Auth::user()->keahlian_id)
                        <form action="{{ route('teknisi_ambil', $item->id) }}" method="post" onsubmit="return confirm('Anda yakin mau ambil pekerjaan ini?');" style="display: inline-block;">
                        @csrf
                        @method('PATCH')
                        <button class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top">Ambil</button>
                        </form>
                        @elseif(Auth::user()->role == 'spv' && empty($item->user->name))
                        <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-warning btn-spv">Pilih Teknisi</a>
                        @elseif (Auth::user()->role == 'teknisi' && ($item->status == 'Upcoming' && $item->keahlian_id == Auth::user()->keahlian_id && $item->user_id == Auth::user()->id))
                        <form action="{{ route('teknisi_mulai_kerja', $item->id) }}" method="post" onsubmit="return confirm('Anda yakin mau mulai pekerjaan ini?');" style="display: inline-block;">
                        @csrf
                        @method('PATCH')
                        <button class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top">Mulai Pekerjaan</button>
                        </form>
                        @elseif ($item->keahlian_id == Auth::user()->keahlian_id && $item->user_id == Auth::user()->id)
                        <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-success btn-hasil-kerja">Upload Hasil Kerja</a>
                        @endif
                      </td> 
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 grid-margin">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">LIST PEKERJAAN CORRECTIVE MAINTENANCE UPCOMING</h4>
              <div class="table-responsive">
                <table class="table" id="table2">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Customer</th>
                      <th>Perangkat</th>
                      <th>Teknisi</th>
                      <th>Nama Pekerjaan</th>
                      <th>Mulai Pekerjaan</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($pekerjaan_berjalan_cm as $item)
                    <tr>
                      <td>{{ $noo++ }}</td>
                      <td>{{ $item->customer->nama_perusahaan }}</td>
                      <td>{{ $item->perangkat->kategori }} - {{ $item->perangkat->perangkat }}</td>
                      <td>{{ $item->user->name ?? null }}</td>
                      <td>{{ $item->nama_pekerjaan }}</td>
                      <td>{{ $item->start_pekerjaan }}</td>
                      <td>
                        @if ($item->status == 'On Progress')
                        <div class="badge badge-outline-warning">On Progress</div>
                        @else
                        <div class="badge badge-outline-primary">{{ $item->status }}</div>
                        @endif
                      </td>
                      <td>
                        @if (Auth::user()->role == 'teknisi' && empty($item->user->name) && $item->keahlian_id == Auth::user()->keahlian_id)
                        <form action="{{ route('teknisi_ambil', $item->id) }}" method="post" onsubmit="return confirm('Anda yakin mau ambil pekerjaan ini?');" style="display: inline-block;">
                        @csrf
                        @method('PATCH')
                        <button class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top">Ambil</button>
                        </form>
                        @elseif(Auth::user()->role == 'spv' && empty($item->user->name))
                        <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-warning btn-spv">Pilih Teknisi</a>
                        @elseif (Auth::user()->role == 'teknisi' && ($item->status == 'Upcoming' && $item->keahlian_id == Auth::user()->keahlian_id && $item->user_id == Auth::user()->id))
                        <form action="{{ route('teknisi_mulai_kerja', $item->id) }}" method="post" onsubmit="return confirm('Anda yakin mau mulai pekerjaan ini?');" style="display: inline-block;">
                        @csrf
                        @method('PATCH')
                        <button class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top">Mulai Pekerjaan</button>
                        </form>
                        @elseif ($item->keahlian_id == Auth::user()->keahlian_id && $item->user_id == Auth::user()->id)
                        <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-success btn-hasil-kerja">Upload Hasil Kerja</a>
                        @endif
                      </td>
                    </tr>
                    @endforeach
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

</div>
@section('modal')

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-spv">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Pilih Teknisi</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('store') }}" method="POST" id="form-spv" enctype="multipart/form-data">
              @method('PATCH')
                @csrf
              <div class="modal-body">
  
              </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                  <button type="button" class="btn btn-primary btn-spv-update">Simpan</button>
              </div>
            </form>
          </div>
      </div>
  </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modal-hasil-kerja">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Upload Hasil Kerja</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('store') }}" method="POST" id="form-hasil-kerja" enctype="multipart/form-data">
              @method('PATCH')
                @csrf
              <div class="modal-body">
  
              </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                  <button type="button" class="btn btn-primary btn-hasil-kerja-update">Simpan</button>
              </div>
            </form>
          </div>
      </div>
  </div>

    @endsection
    @endsection
    @push ('page-scripts')
    @include('pekerjaan.js.ambil_pekerjaan-js')
    @endpush

    