<div class="row">
    <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Jadwal</h6>
            <p class="mb-0"><img src="{{ $kontrak->foto_jadwal }}" width="300" height="200"></p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Nama Perusahaan</h6>
            <p class="mb-0">{{ $kontrak->customer->nama_perusahaan }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Nomor Telepon</h6>
            <p class="mb-0">{{ $kontrak->customer->no_telp }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Nomor Kontrak</h6>
            <p class="mb-0">{{ $kontrak->no_kontrak }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Periode</h6>
            <p class="mb-0">{{ $kontrak->periode }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Mulai Kontrak</h6>
            <p class="mb-0">{{ $kontrak->start_kontrak }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Selesai Kontrak</h6>
            <p class="mb-0">{{ $kontrak->end_kontrak }}</p>
          </li>
          <li class="list-group-item bg-dark">
            <h6 class="mb-0 mt-1">Status</h6>
            <p class="mb-0">
              @if ( \Carbon\Carbon::now()->format('m/d/Y') > $kontrak->end_kontrak)
                  <span class="badge badge-outline-success">KONTRAK SELESAI</span>
              @elseif (\Carbon\Carbon::now()->format('m/d/Y') < $kontrak->start_kontrak)
                  <span class="badge badge-outline-primary">PENDING</span>
              @else
                  <span class="badge badge-outline-warning">PROSES</span>
              @endif
            </p>
          </li>
        </ul>
      </div>