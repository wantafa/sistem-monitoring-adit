@extends ('layouts.master')
@section('title', 'Data Kontrak')
@section('content')
<div class="page-header">
  <h3 class="page-title">Kontrak</h3>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
      <li class="breadcrumb-item active" aria-current="page">Kontrak</li>
    </ol>
  </nav>
</div>
<button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#exampleModal">Tambah Data</button>
<div class="card">
  <div class="card-body">
    <h4 class="card-title">Kontrak</h4>
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table id="order-listing" class="table text-light ">
            <thead>
              <tr>
                <th>No</th>
                <th>Jadwal</th>
                <th>Nama Perusahaan</th>
                <th>Mulai Kontrak</th>
                <th>Selesai Kontrak</th>
                <th>Status</th>
                <th>Foto Jadwal</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($kontrak as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td><img src="{{ $item->foto_jadwal }}" width="200" height="300"></td>
                <td>{{ $item->customer->nama_perusahaan }}</td>
                <td>{{ $item->start_kontrak }}</td>
                <td>{{ $item->end_kontrak }}</td>
                <td>
                  @if (\Carbon\Carbon::now()->format('m/d/Y') > $item->end_kontrak)
                  <span class="badge badge-outline-success">KONTRAK SELESAI</span>
                  
                  @elseif (\Carbon\Carbon::now()->format('m/d/Y') < $item->start_kontrak)
                  <span class="badge badge-outline-primary">PENDING</span>
                  @else
                  <span class="badge badge-outline-warning">PROSES</span>
                  @endif
                </td>
                <td>
                  <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-success btn-show"><i class="mdi mdi-eye"></i></a>
                  <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-primary btn-edit"><i class="mdi mdi-table-edit"></i></a>
                  <a href="#" data-id="{{$item->id}}" class="btn-rounded btn-sm btn-danger swal-6 "><i class="mdi mdi-delete-forever"></i></a>
                  <form action="{{ route('kontrak.delete', $item->id) }}" id="delete{{ $item->id }}" method="post" style="display: inline-block;">
                  @method('delete')
                  @csrf        
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @section('modal')

  {{-- MODAL Tambah Data--}}
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel-2">Tambah Data Kontrak</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('kontrak.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label @error('customer_id') class="text-danger" @enderror>Nama Perusahaan @error('customer_id') | {{ $message }} @enderror</label>
                <select name="customer_id" class="form-control">
                  <option value="" disabled>- Pilih Customer -</option>
                  @foreach ($customer as $item)
                  <option value="{{ $item->id }}">{{ $item->nama_perusahaan }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('no_kontrak') class="text-danger" @enderror>Nomor Kontrak @error('no_kontrak') | {{ $message }} @enderror</label>
                <input id="no_kontrak" name="no_kontrak" placeholder="Masukkan Nomor Kontrak" type="text" class="form-control" value="{{old('no_kontrak')}}">

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('periode') class="text-danger" @enderror>Periode @error('periode') | {{ $message }} @enderror</label>
                <input id="periode" name="periode" placeholder="Masukkan Periode" type="number" class="form-control" value="{{old('periode')}}">

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('start_kontrak') class="text-danger" @enderror>Mulai Kontrak @error('start_kontrak') | {{ $message }} @enderror</label>
                <input id="start_kontrak" name="start_kontrak" type="text" class="form-control" value="{{old('start_kontrak')}}">

              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('end_kontrak') class="text-danger" @enderror>Selesai Kontrak @error('end_kontrak') | {{ $message }} @enderror</label>
                <input id="end_kontrak" name="end_kontrak" type="text" class="form-control" value="{{old('end_kontrak')}}">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label @error('foto_jadwal') class="text-danger" @enderror>Selesai Kontrak @error('foto_jadwal') | {{ $message }} @enderror</label>
                <img id="foto_jadwal" width="200" height="200" class="mb-2"/>
                <input name="foto_jadwal" type="file" class="form-control" onchange="preview1()">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Simpan</button>
          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
        </div>
        </form>
        </div>
      </div>
    </div>
  </div>

  {{-- MODAL LIHAT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-lihat">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Kontrak</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('store') }}" method="POST" id="form-show">
              @csrf
            <div class="modal-body">
              
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
            </div>
          </form>
        </div>
    </div>
</div>

{{-- MODAL EDIT DATA --}}
<div class="modal fade" tabindex="-1" role="dialog" id="modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Kontrak</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('store') }}" method="POST" id="form-edit" enctype="multipart/form-data">
            @method('PATCH')
              @csrf
            <div class="modal-body">

            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary btn-update">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>
  <!-- Modal Ends -->
@endsection
@endsection
@push ('page-scripts')
@include('kontrak.js.kontrak-js')
@endpush