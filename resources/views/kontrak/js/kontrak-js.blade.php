<script>
    @if($errors->any())
        $('#exampleModal').modal('show')
    @endif
  
    $(".swal-6").click(function(e) {
      // id = e.target.dataset.id;
      id = $(this).data('id')
    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil di  Hapus :)', {
          icon: 'success',
        });
        $(`#delete${id}`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
  });
  
        $('.btn-edit').on('click', function() {
          let id = $(this).data('id')
          $.ajax({
            url:`/kontrak/${id}/edit`,
            method:"GET",
            success: function(data) {
              $('#modal-edit').find('.modal-body').html(data)
              $('#modal-edit').modal('show')
            },
            error:function(error){
              console.log(error)
            }
          })
        })
  
        $('.btn-update').on('click', function() {
          let id = $('#form-edit').find('#id_data').val()
          var foto = $('#foto').val()
          var formData = new FormData($('#form-edit')[0]);
  
          // let formData = $('#form-edit').serialize()
          $.ajaxSetup({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
  
          $.ajax({
            type:"POST",
            url:`/kontrak/update/${id}`,
            data:formData,
            contentType: false,
            processData: false,
            success: function(data) {
              swal('Data Kontrak Telah diubah :)','', {
                    timer: 2000,
                    icon: 'success',
                });
              // $('#modal-edit').find('.modal-body').html(data)
              $('#modal-edit').modal('hide')
              window.location.assign('/kontrak')
            },
            error: function (data) {
                      $('#customer_idError').addClass('d-none');
                      $('#alamatError').addClass('d-none');
                      $('#no_telpError').addClass('d-none');
                      $('#start_kontrakkError').addClass('d-none');
                      $('#end_kontrakkError').addClass('d-none');
                      var errors = data.responseJSON;
                      if($.isEmptyObject(errors) == false) {
                          $.each(errors.errors,function (key, value) {
                              var ErrorID = '#' + key +'Error';
                              $(ErrorID).removeClass("d-none");
                              $(ErrorID).text(value)
                          })
                      }
                  }
          })
        })
  
        $('.btn-show').on('click', function() {
          let id = $(this).data('id')
          $.ajax({
            url:`/kontrak/show/${id}`,
            method:"GET",
            success: function(data) {
              $('#modal-lihat').find('.modal-body').html(data)
              $('#modal-lihat').modal('show')
            },
            error:function(error){
              console.log(error)
            }
          })
        })
  </script>

<script>
  $(function() {
    $('input[name="start_kontrak"]').daterangepicker({
      singleDatePicker: true,
    showDropdowns: true,
    minYear: 2000,
    autoApply: true,
    // maxYear: parseInt(moment().format('YYYY'),10)
  });

    $('input[name="end_kontrak"]').daterangepicker({
      singleDatePicker: true,
    showDropdowns: true,
    minYear: 2000,
    autoApply: true,
    // maxYear: parseInt(moment().format('YYYY'),10)
  });
  });
  </script>

{{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
  function preview1() {
    foto_jadwal.src=URL.createObjectURL(event.target.files[0]);
}
</script>