 <div class="row">
     <div class="col-md-6">
         <div class="form-group">
          <input type="hidden" value="{{ $kontrak->id }}" id="id_data"/>
             <label>Nama Perusahaan</label> <b><span class="text-danger" id="customer_idError"></span></b>
             <select name="customer_id" class="form-control">
                 <option value="{{ $kontrak->customer_id }}">{{ $kontrak->customer->nama_perusahaan }}</option>
                 @foreach ($customer as $item)
                 <option value="{{ $item->id }}">{{ $item->nama_perusahaan }}</option>
                 @endforeach
              </select>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Nomor Kontrak</label> <b><span class="text-danger" id="no_kontrakError"></span></b>
             <input id="no_kontrak" name="no_kontrak" placeholder="Masukkan Nomor Kontrak" type="text"
                 class="form-control" value="{{$kontrak->no_kontrak}}" required>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Periode</label> <b><span class="text-danger" id="periodeError"></span></b>
             <input id="periode" name="periode" placeholder="Masukkan Periode" type="number"
                 class="form-control" value="{{$kontrak->periode}}" required>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Mulai Kontrak</label> <b><span class="text-danger" id="periodeError"></span></b>
             <input id="start_kontrak" name="start_kontrak" placeholder="Masukkan Mulai Kontrak" type="text"
                 class="form-control" value="{{$kontrak->start_kontrak}}" required>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Selesai Kontrak</label> <b><span class="text-danger" id="periodeError"></span></b>
             <input id="end_kontrak" name="end_kontrak" placeholder="Masukkan Selesai Kontrak" type="text"
                 class="form-control" value="{{$kontrak->end_kontrak}}" required>
         </div>
     </div>
     <div class="col-md-6">
              <div class="form-group">
                <label @error('foto_jadwal') class="text-danger" @enderror>Selesai Kontrak @error('foto_jadwal') | {{ $message }} @enderror</label>
                <img id="foto_jadwal1" width="200" height="200" class="mb-2"/>
                <input name="foto_jadwal" type="file" class="form-control" onchange="preview2()">
              </div>
            </div>
     <script>
        $(function() {
          $('input[name="start_kontrak"]').daterangepicker({
            singleDatePicker: true,
          showDropdowns: true,
          minYear: 2000,
          autoApply: true,
          maxYear: parseInt(moment().format('YYYY'),10)
        });
      
          $('input[name="end_kontrak"]').daterangepicker({
            singleDatePicker: true,
          showDropdowns: true,
          minYear: 2000,
          autoApply: true,
          maxYear: parseInt(moment().format('YYYY'),10)
        });
        });
        </script>

<script>
  function preview2() {
    foto_jadwal1.src=URL.createObjectURL(event.target.files[0]);
}
</script>