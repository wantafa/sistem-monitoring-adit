<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePekerjaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pekerjaan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('perangkat_id');
            $table->integer('user_id');
            $table->string('bagian', 20);
            $table->string('nama_pekerjaan', 150);
            $table->string('kategori', 30);
            $table->string('status', 20);
            $table->string('foto_selfie', 150);
            $table->string('foto_kerja', 150);
            $table->string('report_fisik', 150);
            $table->string('keterangan');
            $table->string('start_pekerjaan', 20);
            $table->string('end_pekerjaan', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pekerjaan');
    }
}
