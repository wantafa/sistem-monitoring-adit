<?php

use App\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect(route('login'));
});
// Route::get('/index', function () {
//     return view('barang.blank');
// });

Route::group(['middleware' => 'auth'], function() {

    // PERANGKAT
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/perangkat', 'PerangkatController@index')->name('perangkat');
    Route::post('/perangkat', 'PerangkatController@store')->name('store');
    Route::get('/perangkat/{perangkat}/edit', 'PerangkatController@edit')->name('edit');
    Route::patch('/perangkat/update/{perangkat}', 'PerangkatController@update')->name('update');
    Route::get('/perangkat/show/{perangkat}', 'PerangkatController@show')->name('show');
    Route::delete('/perangkat/{perangkat}', 'PerangkatController@destroy')->name('perangkat.delete');

    // CUSTOMER
    Route::get('/customer', 'CustomerController@index')->name('customer');
    Route::post('/customer', 'CustomerController@store')->name('customer.store');
    Route::get('/customer/{customer}/edit', 'CustomerController@edit')->name('edit');
    Route::patch('/customer/update/{customer}', 'CustomerController@update')->name('update');
    Route::get('/customer/show/{customer}', 'CustomerController@show')->name('show');
    Route::delete('/customer/{customer}', 'CustomerController@destroy')->name('customer.delete');
    
    // KONTRAK
    Route::get('/kontrak', 'KontrakController@index')->name('kontrak');
    Route::post('/kontrak', 'KontrakController@store')->name('kontrak.store');
    Route::get('/kontrak/{kontrak}/edit', 'KontrakController@edit')->name('edit');
    Route::patch('/kontrak/update/{kontrak}', 'KontrakController@update')->name('update');
    Route::get('/kontrak/show/{kontrak}', 'KontrakController@show')->name('show');
    Route::delete('/kontrak/{kontrak}', 'KontrakController@destroy')->name('kontrak.delete');
    
    // TEKNISI
    Route::get('/teknisi', function () {
        // $teknisi = User::where('role', 'teknisi')->get();
        $teknisi = DB::table('users')
        ->join('keahlian', 'users.keahlian_id', '=', 'keahlian.id')
        ->where('users.role', '=', 'teknisi')
        ->select('users.*','keahlian.keahlian')->get();
        $no = 1;
      // dd($teknisi);
        return view('manajemen_user.teknisi', compact('teknisi','no'));
     })->name('teknisi');
    
    // PEKERJAAN
    Route::get('/pekerjaan', 'PekerjaanController@index')->name('pekerjaan');
    Route::post('/pekerjaan', 'PekerjaanController@store')->name('pekerjaan.store');
    Route::get('/pekerjaan/{pekerjaan}/edit', 'PekerjaanController@edit')->name('edit');
    Route::get('/pekerjaan/spv_ambil/{pekerjaan}', 'PekerjaanController@spv_form_ambil')->name('spv');
    Route::patch('/pekerjaan/update/{pekerjaan}', 'PekerjaanController@update')->name('update');
    Route::get('/pekerjaan/show/{pekerjaan}', 'PekerjaanController@show')->name('show');
    Route::delete('/pekerjaan/{pekerjaan}', 'PekerjaanController@destroy')->name('pekerjaan.delete');
    Route::patch('/pekerjaan/teknisi_ambil/{pekerjaan}', 'PekerjaanController@teknisi_ambil')->name('teknisi_ambil');
    Route::patch('/pekerjaan/spv_ambil/{pekerjaan}/update', 'PekerjaanController@spv_ambil')->name('spv_ambil');
    Route::get('/pekerjaan/hasil_kerja/{pekerjaan}', 'PekerjaanController@form_hasil_kerja');
    Route::patch('/pekerjaan/teknisi_mulai_kerja/{pekerjaan}', 'PekerjaanController@teknisi_mulai_kerja')->name('teknisi_mulai_kerja');
    
    Route::patch('/pekerjaan/upload_hasil_kerja/{pekerjaan}', 'HomeController@hasil_kerja');
    
    Route::get('/laporan', 'PekerjaanController@laporan')->name('laporan.pekerjaan');
    Route::get('/laporan_pekerjaan/show/{pekerjaan}', 'PekerjaanController@laporan_foto')->name('foto.laporan');

    Route::get('/cetak_laporan/{pekerjaan}', 'PekerjaanController@cetak_laporan')->name('cetak_laporan');
    
  // Profile
    Route::get('profile', 'Admin\UserController@profile')->name('profile');
    Route::post('profile', 'Admin\UserController@update')->name('updateProfile');
    Route::post('profile/image', 'Admin\UserController@ubahfoto')->name('ubahFoto');
    Route::patch('profile/password', 'Admin\UserController@ubahPassword')->name('ubahPassword');

  // Manajemen User
  Route::get('/manajemen_user', 'UserController@index')->name('manajemen_user');
  Route::post('/manajemen_user', 'UserController@store')->name('manajemen_user.store');
  Route::get('/manajemen_user/{manajemen_user}/edit', 'UserController@edit')->name('edit');
  Route::patch('/manajemen_user/update/{manajemen_user}', 'UserController@update')->name('update');
  Route::get('/manajemen_user/show/{manajemen_user}', 'UserController@show')->name('show');
  Route::delete('/manajemen_user/{manajemen_user}', 'UserController@destroy')->name('manajemen_user.delete');
});



