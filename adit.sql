-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2023 at 07:48 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adit`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_perusahaan` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_bagian` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `nama_perusahaan`, `nama_bagian`, `alamat`, `no_telp`, `created_at`, `updated_at`) VALUES
(1, 'PT CAPIL', 'PIAK', 'JAKARTA', '123123213', '2021-11-08 12:04:27', '2021-11-08 12:04:27'),
(3, 'Ullamco aliqua Dolo', 'Accusantium corporis', 'Eligendi ea et ullam', '27', '2021-11-09 03:41:37', '2021-11-09 03:41:37'),
(4, 'Est tempore non am', 'Exercitation sed con', 'Ea aliquip beatae el', '41', '2021-11-09 03:41:51', '2021-11-09 03:41:51');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `keahlian`
--

CREATE TABLE `keahlian` (
  `id` int(11) NOT NULL,
  `keahlian` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keahlian`
--

INSERT INTO `keahlian` (`id`, `keahlian`) VALUES
(1, 'Mechanical'),
(2, 'Electrical'),
(3, 'Plumbing');

-- --------------------------------------------------------

--
-- Table structure for table `kontrak`
--

CREATE TABLE `kontrak` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `no_kontrak` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `periode` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_kontrak` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_kontrak` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kontrak`
--

INSERT INTO `kontrak` (`id`, `customer_id`, `no_kontrak`, `periode`, `start_kontrak`, `end_kontrak`, `created_at`, `updated_at`) VALUES
(1, 1, 'ZWY2601', '2020', '11/05/2021', '11/10/2021', '2021-11-09 03:54:11', '2021-11-09 08:39:16'),
(2, 3, 'ZWY2601', '2017', '11/10/2021', '11/20/2021', '2021-11-09 04:13:49', '2021-11-09 04:13:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_22_143924_create_notes_table', 2),
(5, '2020_10_22_144046_create_subjects_table', 2),
(6, '2020_11_23_124053_add_column_to_users_table', 3),
(7, '2020_11_23_144355_create_post_table', 4),
(8, '2021_05_23_105316_create_perpustakaan_table', 5),
(9, '2021_05_23_105820_create_arsip_table', 5),
(10, '2021_05_23_114513_create_profil_table', 5),
(11, '2021_05_23_114754_create_renstra_table', 5),
(12, '2021_05_23_114854_create_renkerta_table', 5),
(13, '2021_05_23_115031_create_lakip_table', 5),
(14, '2021_05_23_115307_create_pengadaan_table', 5),
(15, '2021_09_18_141545_create_kontak_table', 6),
(16, '2021_11_08_151438_create_customer_table', 7),
(17, '2021_11_08_154201_create_pekerjaan_table', 8),
(18, '2021_11_08_200801_create_kontrak_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE `pekerjaan` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `perangkat_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `bagian` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pekerjaan` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Upcoming',
  `foto_selfie` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_kerja` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_fisik` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_pekerjaan` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_pekerjaan` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id`, `customer_id`, `perangkat_id`, `user_id`, `bagian`, `nama_pekerjaan`, `kategori`, `status`, `foto_selfie`, `foto_kerja`, `report_fisik`, `keterangan`, `start_pekerjaan`, `end_pekerjaan`, `created_at`, `updated_at`) VALUES
(1, 4, 5, 1, 'Placeat illo est d', 'Quo ut perferendis e', 'Preventive Maintenance', 'On Progress', NULL, NULL, NULL, NULL, '11/07/2021', '11/24/2021', '2021-11-09 09:28:04', '2021-11-10 08:29:20'),
(2, 3, 3, 3, 'Dignissimos minus fa', 'Amet amet quis vol', 'Corrective Maintenance', 'Selesai', '/uploads/images/pekerjaan/Dzakwan_foto_selfie1636618418.jpg', '/uploads/images/pekerjaan/Dzakwan_foto_kerja1636618418.jpg', '/uploads/images/pekerjaan/Dzakwan_report_fisik1636618418.jpg', 'Bismillah lagi', '11/08/2021', '11/23/2021', '2021-11-09 09:29:16', '2021-11-11 08:13:38');

-- --------------------------------------------------------

--
-- Table structure for table `perangkat`
--

CREATE TABLE `perangkat` (
  `id` int(11) UNSIGNED NOT NULL,
  `kategori` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perangkat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perangkat`
--

INSERT INTO `perangkat` (`id`, `kategori`, `perangkat`, `created_at`, `updated_at`) VALUES
(1, 'Electrical', 'Air Conditioner', '2021-11-02 08:40:44', '2021-11-06 09:33:38'),
(2, 'Electrical', 'PAC (Precisiion Air Conditioner)', '2021-11-04 13:51:19', '2021-11-06 09:33:56'),
(3, 'Mechanical', 'UPS (Uninterruptible Power Supply)', '2021-11-04 22:05:49', '2021-11-06 09:35:05'),
(4, 'Electrical', 'Genset', '2021-11-04 22:08:04', '2021-11-06 09:35:24'),
(5, 'Umum', 'CCTV (Close Circuit Television)', '2021-11-04 22:14:10', '2021-11-06 09:36:47'),
(8, 'Umum', 'Access Control', '2021-11-06 06:14:53', '2021-11-06 09:37:12'),
(9, 'Mechanical', 'Genset', '2021-11-06 09:34:16', '2021-11-06 09:34:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `keahlian_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '/uploads/images/profile/default.png',
  `alamat` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `keahlian_id`, `name`, `username`, `email`, `role`, `image`, `alamat`, `no_hp`, `jenis_kelamin`, `status`, `email_verified_at`, `password`, `remember_token`, `last_login`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Admin', 'admin', 'admin@admin.com', 'administrator', '/uploads/images/profile/Dzakwan_1606300468.jpeg', NULL, NULL, NULL, NULL, NULL, '$2y$10$8eBuhFN5vlgPdtQl0VL9z.x5ozUT.rqdw0nE8Dw7rduumEb/rk6de', NULL, '2021-12-04 09:17:42', '2020-10-01 21:26:07', '2021-12-04 02:17:42'),
(2, NULL, 'Shinta', 'shinta', 'shinta@admin.com', 'spv', '/uploads/images/profile/default.png', NULL, NULL, NULL, 'ada', NULL, '$2y$10$8eBuhFN5vlgPdtQl0VL9z.x5ozUT.rqdw0nE8Dw7rduumEb/rk6de', NULL, '2021-11-10 11:42:07', '2020-10-04 06:40:03', '2021-11-10 04:42:07'),
(3, 1, 'Riski', 'riski', 'riski@admin.com', 'teknisi', '/uploads/images/profile/default.png', NULL, NULL, NULL, 'kerja', NULL, '$2y$10$8/yhNvxSAF6PJcxe4R4y3OJw6IFbpFpXtrptO.Sw9yj6lVP/3CUUW', NULL, NULL, '2020-11-24 07:50:11', '2021-09-09 01:24:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keahlian`
--
ALTER TABLE `keahlian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontrak`
--
ALTER TABLE `kontrak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perangkat`
--
ALTER TABLE `perangkat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `keahlian`
--
ALTER TABLE `keahlian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kontrak`
--
ALTER TABLE `kontrak`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `perangkat`
--
ALTER TABLE `perangkat`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
